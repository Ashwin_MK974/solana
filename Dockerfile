FROM ubuntu:latest
MAINTAINER ashwin
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
&& apt install wget -y \
&& wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solana-env-linux.v2.sh \
&& chmod +x solana-env-linux.v2.sh \
&& bash solana-env-linux.v2.sh \
&& . ~/.bashrc
