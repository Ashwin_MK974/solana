#!/bin/bash
#@Author Ashwin MK 


#Pour le fichier Anchor.toml
STATE_FILE1="/etc/solger"
MERGE_ORIGIN1="/etc/solger/sandbox/myproject/Anchor.toml"
MERGE_DEST1="/etc/solger/sandbox/solana-crud-project/Anchor.toml"
CRITICAL_FINGER1="/etc/solger/sandbox/solana-crud-project/"
CRITICAL_SEQUENCES1="libAnchorDone"



#Pour le fichier solana-crud-project.js
STATE_FILE2="/etc/solger"
MERGE_ORIGIN2="/etc/solger/sandbox/myproject/tests/myproject.js"
MERGE_DEST2="/etc/solger/sandbox/solana-crud-project/tests/solana-crud-project.js" #C'est bien myproject.js
CRITICAL_FINGER2="/etc/solger/sandbox/solana-crud-project/"
CRITICAL_SEQUENCES2="libJsDone"


#Pour le fichier package.json
STATE_FILE3="/etc/solger"
MERGE_ORIGIN3="/etc/solger/sandbox/myproject/package.json"
MERGE_DEST3="/etc/solger/sandbox/solana-crud-project/package.json" #C'est bien myproject.js
CRITICAL_FINGER3="/etc/solger/sandbox/solana-crud-project/"
CRITICAL_SEQUENCES3="libPackageDone"


#(cd /etc/solger/sandbox/solana-crud-project ; rm -rf node_modules)
#merge_project  "$STATE_FILE1" "$MERGE_ORIGIN1" "$MERGE_DEST1" "$CRITICAL_FINGER1" "$CRITICAL_SEQUENCES1"
#merge_project  "$STATE_FILE2" "$MERGE_ORIGIN2" "$MERGE_DEST2" "$CRITICAL_FINGER2" "$CRITICAL_SEQUENCES2"
#merge_project  "$STATE_FILE3" "$MERGE_ORIGIN3" "$MERGE_DEST3" "$CRITICAL_FINGER3" "$CRITICAL_SEQUENCES3"
