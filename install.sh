#!/bin/bash
function get__f(){
  ( cd /etc/solger ; wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solger.sh && chmod +x solger.sh )
}
if [ -d "/etc/solger" ]
then
  echo "Solger est déjà installé"
  echo "Utilisé la commande solger-repair pour reinstaller le Solger"
else
 echo "Installation en cours"
  mkdir /etc/solger;
  get__f
fi

if grep -Fxq "#solger:true" ~/.bashrc
then
    echo "Alias already install"
else
    echo "#solger:true">> ~/.bashrc;
    echo "Creation des alias pour Solger";
    echo "alias edit-a='nano ~/.bashrc'">> ~/.bashrc;
    echo "alias solger-repair='bash /etc/solger-restart-init.sh failled_init_solger'" >> ~/.bashrc;
    echo "alias sr='nano /etc/solger/solger_state'" >> ~/.bashrc;
    echo "alias src='source ~/.bashrc'">> ~/.bashrc;
    echo "alias terra='terraform apply -auto-approve'">> ~/.bashrc;
    echo "alias solger='bash /etc/solger/./solger.sh solger_init'" >> ~/.bashrc;
    echo "alias solger-read='cat /etc/solger/solger.sh'" >> ~/.bashrc;
    echo "#solger end alias">> ~/.bashrc;
    source ~/.bashrc;
fi
echo "Installation de Solger reussie !"
echo "Mise à jour de solger"
bash /etc/solger/solger.sh update_struct


