#!/bin/bash
#@Author Ashwin MK
#Fichier initiale permettant la restauration du programme Solger 
#Version 1.1 Testing
function success_write(){
    echo ""
    echo -e "\e[1m$1 ✔️\e[0m"
    echo -e "\e[97m"
}
function fail_write(){
   echo ""
   echo -e "\e[1m$1 ❌\e[0m"
   echo -e "\e[97m"
}
function green_write(){
    echo ""
    echo -e "\e[32m$1"
    echo -e "\e[97m"
}
function green_write_sucess(){
    echo ""
    echo -e "\e[32m$1✔️"
    echo -e "\e[97m"
}

function important_write(){
    echo ""
    echo -e "\e[1m$1\e[0m"
    echo -e "\e[97m"
}

function get_answer() 
{
     echo ""
    echo -e "\e[1m$1 🔍\e[0m"
    echo -e "\e[97m"   
}

function warning_write(){
    echo ""
    echo -e "\e[1m""⚠️ "  $1 "\e[0m"
    echo -e "\e[97m"
}
function italic_write(){
    echo ""
    echo -e "\033[3m$1\033[23m"
    echo -e "\e[97m"
}
function italic_warning_write(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}
function italic_write_warning(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}
function failled_init_solger(){
function info_install(){
                    important_write  "Reinstallation uniquement du script ainsi que des dépendances nécessaires"
                    success_write "L'espace de travail actuelle n'est pas impacté"
}
function install(){
               warning_write "Installation de Solger en cours"  &&  (cd  /etc ; mkdir solger) &&  ( cd /etc/solger ; wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solger.sh && chmod +x solger.sh ) && green_write_sucess "Installation de Solger réeussie "    && warning_write "Mise à jour de Solger"  && bash /etc/solger/solger.sh update_struct  && green_write_sucess "Mise à jour de Solger réussie"
}

function download_install(){
                ( cd /etc/solger ; wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solger.sh && chmod +x solger.sh ) && green_write_sucess "Installation de Solger réeussie "    && warning_write "Mise à jour de Solger"  && bash /etc/solger/solger.sh update_struct  && green_write_sucess "Mise à jour de Solger réussie"
}
if [ -d /etc/solger ]
 then
        warning_write "Installation de Solger détecter"
        if [ -e /etc/solger/solger.sh ]
        then
                success_write "Détection du script solger.sh"
                if [  "$1" == "--force" ]
                then
                    warning_write "Voulez-vous reinstaller Solger complètement ? Cela supprimera tout le travail effectué ainsi que les sauvegardes"
                    read action
                                    if  [ "$action" == "y" ]
                                    then
                                        important_write "Suppresion de  Solger en cours ..."  &&  rm -rf /etc/solger &&  install
                                    else
                                        green_write_sucess "Opération annulé"
                                    fi
                else
                                info_install  &&   (cd /etc/solger ; rm solger.sh) && download_install
                fi
        else
            fail_write  "Installation détecté mais le script solger.sh est inexistant"
           info_install
            download_install
        fi
    else
            install
    fi
}

"$@"
