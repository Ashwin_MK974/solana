#!/bin/bash
#@Author Ashwin MK
#Maj => Correction de wallet verificator avec une fonctionalité speciale d'envoie de fond en mode devnet de facon modéré (2sol)
#Implémentation de super_validator pour démarer uniquement qu'en cas de besoin afin d'améliorer la rapidité
#Implémentation d'une fonction permettent de renitialiser le cluster 
function set_env(){
        if [[ $1 == "--localhost"   || $1 == "--localnet" ]]
        then        
                    green_write_sucess "Changement en localnet dans le fichier Anchor.toml"
                    sed -i 's/devnet/localnet/g' /etc/solger/sandbox/solana-crud-project/Anchor.toml
                    if  verify_existant_element__v3  e /etc/solger/sandbox/frontend/myproject/src/App.js 0 1
                    then
                            important_write "Changement dans le fichier App.js"
                            sed -i "s/clusterApiUrl('devnet')/'http:\/\/127.0.0.1:8899'/g" /etc/solger/sandbox/frontend/myproject/src/App.js
                    fi
                    solana config set --url localhost && return 0
        elif [[ "$1" == "--devnet" ]]
        then
                   green_write_sucess "Changement en devnet dans le fichier Anchor.toml"
                   sed -i 's/localnet/devnet/g' /etc/solger/sandbox/solana-crud-project/Anchor.toml
                    if  verify_existant_element__v3  e /etc/solger/sandbox/frontend/myproject/src/App.js 0 1
                    then
                            important_write "Changement dans le fichier App.js"
                             sed -i "s/'http:\/\/127.0.0.1:8899'/clusterApiUrl('devnet')/g" /etc/solger/sandbox/frontend/myproject/src/App.js
                    fi
                   solana config set --url devnet && return 0
        else
            echo "Solger Command Error from set_env => Vérifier les commandes fournies"
        fi

}
function detect_solana_test_validator(){
    if [ $(ps aux | grep solana-test-validator | wc -l) -gt 1 ]
    then
            green_write_sucess "Solana test validator est démarrré"
            return $1
        else
            echo "Solana-test-validator n'est pas activé"
            return $2
        fi
}
function exec_deployment()
{
    ( cd $1 ; warning_write "Déploiement sur la $2" &&  anchor deploy && green_write_sucess "Déploiement sur la $2 réussie"  ) && return 0
}
#exec_deployment /etc/solger/sandbox/solana-crud-project devnet


function deploy__on__devnet(){
    important_write "Déploiement du programme en Devnet"
    echo $1;
    echo $2
    set_env --devnet
    if detect_solana_test_validator 0 1 #Si le validateur est en marche
    then
       warning_write "Exec 1"
       exec_deployment $1 $2 
    else #Si le validateur n'est pas en marche
       warning_write "Exec 2" 
        solana_validator_manager start  &&  warning_write "En attente du démarrage du validateur ⌛ ...." &&  sleep 1 &&  exec_deployment $1 $2
    fi
}



function deploy__on__localhost(){
        #Le validateur en soit sur le localhost peut toujours être actif
        important_write "Déploiement du programme en localnet"
        if  detect_solana_test_validator 0 1
        then
            exec_deployment $1 $2
        else
            solana_validator_manager start && warning_write "En attente du démarrage du validateur ⌛ ...." &&  sleep 1 && exec_deployment $1 $2
        fi 
}

function solana_validator_manager(){
        if [[ "$1" == "start" ]]
        then 
            important_write "Démarrage de solana-test-validator en arrière plan"
            solana-test-validator </dev/null &>/dev/null &
            sleep 7
            success_write "solana-test-validator est démarré"
        elif [[ "$1" == "stop" ]]
        then
            warning_write "Arrêt de solana-test-validator"
            kill $(pidof solana-test-validator)
            success_write "solana-test-validator est complètement arrêté"
        elif [[ "$1" == "reset" ]]
        then
           important_write "Rinitialisation des données du cluster" && 
           if [ "$(pidof solana-test-validator)" != " " ]
           then
           kill $(pidof solana-test-validator)
           solana-test-validator --reset && success_write "Cluster renitialisé avec succès"
           fi
        fi
}

#


state_repair_testing=$((0))
function repair_before_test(){ #Programme avec de la gestion d'état pour éviter les boucles d'erreurs ...
    if [ $state_repair_testing -eq 0 ]
    then
        get_answer "Tentative de résolution du problème"
        node /etc/solger/loader.js
        important_write "Exécution de NPM RESOLV BUILD ..."
        resolv_struct npm --build
        state_repair_testing=$((state_repair_testing+1))
        return 0
    else
        important_write "Impossible de résoudre le problème ..."
        get_answer "Veuillez entrer votre problème sur le discord de Solana"
        return 1
    fi

}
#L'exécution des tests requiet à ce que le validateur est en arret
function path_to_test(){
        important_write  "L'environnement d'exécution d'anchor est $1"
        repair_before_test $1
        (cd $1 ; warning_write "Test du projet par Anchor ...."  &&  anchor test && success_write "Test du projet par Anchor réussie") && return 0 || fail_write "Anchor à rencontré une erreur lors des test sur le projet" && return 1
    }

function testing() { 
    important_write "Modification de l'environnement  Anchor test sur localnet pour la rapidité"
    set_env --localnet
    if detect_solana_test_validator 0 1 #Si le validateur est en marche
    then
       #warning_write "Exec 1"
       solana_validator_manager stop
       path_to_test $1
       solana_validator_manager start
    else #Si c'est déjà en arret on à juste alors qu'a déployer
         #warning_write "Exec 2" 
         path_to_test $1
         solana_validator_manager start
    fi
    important_write "Restauration de l'environnement de déploiement sur $2"
    set_env $2 && green_write_sucess "Restauration de la valeur de l'environnement de déploiement réussie"
}

#Wallet verificator vérifie qu'on à bien une addresse valide avec les fonds suffisants avant de déploieyer quelque soit l'environnement
function wallet_verificator(){
    if grep -Fxq "solana{config_local_wallet:true}" /etc/solger/solger_state
    then
        success_write "Portefeuille locale configuré"
        italic_write "L'addresse locale du Wallet est $(solana address) "
    else
        echo "Configuration du wallet Solana en cours"   
        "" | solana-keygen new --force
        echo "Configuration du Wallet Solana terminé"
        echo "Mise à jour du fichier Solger State"
        verify_state solana{config_local_wallet:true}
        echo "Fichier Solger State Mise à jour"
    fi 
    set_env $1
    #CHECK_BALANCE=$( solana balance | sed  -r 's/(.{1}).*/\1/' )
    if [[ "$1" == "--devnet" ]]
    then
        important_write "Exécution de wallet verificator en mode $1"
        if [ $((CHECK_BALANCE)) -gt 2 ]
        then
            green_write_sucess "Font suffisant pour le déploiement"
        else
            important_write "Ajout de fonds dans le wallet pour le déploiement"
            super_validator start
            solana airdrop 2 && green_write_sucess "Font ajouté avec succès"
        fi
    else
        important_write "Exécution de wallet verificator en mode $1"
        if [ $((CHECK_BALANCE)) -gt 2 ]
        then
            green_write_sucess "Font suffisant pour le déploiement"
        else
            important_write "Ajout de fonds dans le wallet pour le déploiement"
            super_validator start
            solana airdrop 100 && green_write_sucess "Font ajouté avec succès"
        fi
    fi

}
function super_validator(){
    if [ "$1" == "start" ]
    then
            if  detect_solana_test_validator 0 1 > /dev/null 2>&1
            then 
                echo ""
            else
                solana_validator_manager start > /dev/null 2>&1
                sleep 7
            fi
    else
            if  detect_solana_test_validator 0 1 > /dev/null 2>&1
            then 
                solana_validator_manager stop > /dev/null 2>&1
            else
                echo ""
            fi
    fi
}
#wallet_verificator
#testing /etc/solger/sandbox/solana-crud-project

#Si le validateur n'est pas démaré alors on le démarre, utilisé ici pour un déploiement devnet
#detect_solana_test_validatosrs 0 1 &&  green_write_sucess "Anchor deploy" && solana_validator_manager stop
#detect_solana_test_validatosrs 1 0 && solana_validator_manager start && green_write_sucess "Anchor deploy" && solana_validator_manager stop
