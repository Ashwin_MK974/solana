#!/bin/bash
#@Author Ashwin MK 
VERSION="1.0.4.11 Jelly Everest Star  ✵ ✯ 🌠 "
#Chargement du script dediée aux déploiement des programmes sur le réseau
source /etc/solger/./deployment.sh
source /etc/solger/variables.sh #Charge les variables utilisé par le programme
function success_write(){
    echo ""
    echo -e "\e[1m$1 ✔️\e[0m"
    echo -e "\e[97m"
}
function fail_write(){
   echo ""
   echo -e "\e[1m$1 ❌\e[0m"
   echo -e "\e[97m"
}
function green_write(){
    echo ""
    echo -e "\e[32m$1"
    echo -e "\e[97m"
}
function green_write_sucess(){
    echo ""
    echo -e "\e[32m$1✔️"
    echo -e "\e[97m"
}

function important_write(){
    echo ""
    echo -e "\e[1m$1\e[0m"
    echo -e "\e[97m"
}

function get_answer() 
{
     echo ""
    echo -e "\e[1m$1 🔍\e[0m"
    echo -e "\e[97m"   
}

function warning_write(){
    echo ""
    echo -e "\e[1m""⚠️ "  $1 "\e[0m"
    echo -e "\e[97m"
}
function italic_write(){
    echo ""
    echo -e "\033[3m$1\033[23m"
    echo -e "\e[97m"
}
function italic_warning_write(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}
function italic_write_warning(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}
function increment_backup(){
    find -type f -exec md5sum '{}' \; > md5sum.txt
}
function read_state_backup(){
    echo ""
}
function verify_existant_element(){
    if [ -$1 /etc/solger/$2 ]
    then
        success_write "L'élement'$2 existe"
        return $3
    else
        fail_write "L'élement $2 n'existe pas"
        return $4
    fi
}
#verify_existant_element e solana-img.tar 1 1



function verify_existant_element__v2(){
    if [ -$1 $2$3 ]
    then
        success_write "L'élement $3 existe"
        return $4
    else
        fail_write "L'élement $3 n'existe pas"
        return $5
    fi
}

function verify_existant_element__v3(){
    if [ -$1 $2 ]
    then
        success_write "L'élement $2 existe"
        return $3
    else
        fail_write "L'élement $2 n'existe pas"
        return $4
    fi
}
#verify_existant_element__v2 e /etc/solger/ loader.js 0 1 && warning_write "Success"
#Pour un dossier => verify_existant_element__v2 d /etc/solger/ dossier 0 1





function config_stack(){
    if eval $1 ; then
        success_write "Execute en mode success"
        eval $2
    else
        success_write "Execute en mode fail"
        eval $2
    fi
}
function verify_state(){
                    #Mise à jour du fichier state pour les exécution des prochaines fonctions
                    if [ -e /etc/solger/solger_state ]
                    then
                                    if grep -Fxq $1 /etc/solger/solger_state
                                    then
                                            green_write_sucess "Ficheir solger state déjà à jour"
                                    else
                                            warning_write "Mise à jour du fichier state"   
                                            important_write "Ecriture de ===> " $1  "== DESTINATION FILE ===> solger_state"
                                            echo $1 >> /etc/solger/solger_state
                                            green_write_sucess "Fichier mise à jour"
                                    fi
                    else
                                    warning_write "Création du fichier solger state"
                                    touch solger_state /etc/solger
                                    important_write "Mise à jour du fichier state"
                                    important_write "Ecriture " $1
                                    echo $1 >> /etc/solger/solger_state
                                    green_write_sucess "Fichier mise à jour"
                    fi
}



#config_stack '[[ "x" == "x" ]]'  "return 0" && fail_write "Fail"


solger_init () 
{
    if [[ $1 == "get" || $1 == "g" ]]
    then
                                get_struct $2 $3 $4 $5 $6 $7;
    elif [[ $1 == "start" || $1 == "s" ]]                
    then
                                start_struct $2 $3 $4
    elif [[ $1 == "directly" || $1 == "d" ]]
    then
                                directly_started_struct $2 $3
    elif [[ $1 == "--update" || $1 == "--u" ]]
    then
                                update_struct
    elif [ $1 == "--version" ]
    then
                                versioning
    elif [ $1 == "--ls" ]
    then
                                solger_ls
    elif [ $1 == "resolv" ]
    then
                                resolv_struct $2 $3
    elif [ $1 == "read" ]
    then
                                read_struct $2 $3
    elif [ $1 == "set-env" ]
    then
                                set_env $2 $3;
    elif [[ $1 == "deploy" || $1 == "dp" ]]
    then
                                deploy_struct $2 $3 $4 $5 $6 $7 ;
    elif [ $1 == "init" ]
    then
                               init_solana_struct $2 $3 $4
    elif [ $1 == "rinit" ]
    then
                               echo "" > /etc/solger/solger_state
    elif [  $1 == "rm" ]
    then
                               erase_function $2
    elif [  $1 == "show" ]
    then
                               show_struct $2
    elif [  $1 == "run" ]
    then
                               start_server_struct $2
    elif [  $1 == "set-env" ]
    then
                               set_env $1
    elif [  $1 == "manage-validator" ]
    then
                               solana_validator_manager $2
    elif [  $1 == "set" ]
    then
                               set_struct $2 $3 $4 $5
    elif [  $1 == "try" ]
    then
                               try_debug $2 $3 $4 $5
    elif [  $1 == "insert" ]
    then
                               insert_struct $2 $3 $4 $5
    elif [  $1 == "compile" ]
    then
                               compile_struct
    elif [  $1 == "install" ]
    then
                               install_struct  $2 $3
    elif [  $1 == "create" ]
    then
                              create $2 $3 $4 $5 $6
    elif [  $1 == "test" ]
    then
                              test $2 $3 $4 $5 $6 $7
    else
        warning_write "Solger Command Error : Commande générale provenant de Solger , veuillez vérifier votre argument 2"
    fi
}

function create()
{
    if [[ "$1" == "new" && "$2" == "solana-project" && "$3" != ""  ]]
    then
        verify_existant_element__v3 d /etc/solger/sandbox/$3 1 0 &&
        mkdir  /etc/solger/sandbox/$3 &&
        (cd /etc/solger/sandbox/$3 ; mkdir src ) &&
        (cd /etc/solger/sandbox/$3/src ; cargo new --lib program ) &&
        italic_write "Création d'un nouveau projet cargo : cargo new --lib program"
    else
        warning_write "Une erreur de syntaxe dans la commande"
        italic_write "Exemple d'utilisation : solger create new solana-project myOneProject"
        
    fi
}

function test()
{
    if [[ "$1" == "solana" && "$2" == "version" && "$3" != "" ]]
    then
        sh -c "$(curl -sSfL https://release.solana.com/v$3/install)"
    else
        warning_write "Une erreur de syntaxe dans la commande"
        italic_write "Exemple d'utilisation : solger test solana version 1.14.9"
        
    fi
}

function start_server_struct(){
    if [ "$1" == "serve" ]
    then
        important_write "Démarrage du serveurn WEB"
        (cd /etc/solger/sandbox/frontend/myproject ; npm run start )
    elif [ "$1" == "test" ]
    then
        super_validator stop
        export BROWSER=
        (cd /etc/solger/sandbox/solana-crud-project ; anchor test)
    else
        fail_write "Erreur provenant de start_server_struct"
    fi
}
function erase_function(){
    echo "1=====>  " $1
    echo "2=====>  "$2

    if [ -e /etc/solger/$1 ]
    then
        if [ $1 == "solger_state" ]
        then
            if [[ $1 == "solger_state"  && $2 == "--force" || $1 == "solger_state"  && $2 == "--f" ]]
            then
                   rm /etc/solger/$1 && success_write "Suppresion du fichier $1 réalisé avec succes" || fail_write "Le fichier $1 n'existe pas"
            else
                    warning_write "La suppression du fichier solger_state peut provoquer une instabilité de Solger"
                    warning_write "Une interraction via le CLI normal sera nécessaire pour la réalisation de certaines actions"
                    important_write "Voulez-vous continuer ? (y) || (n) "
                    read action_delete_solger_state;
                    if [[ $action_delete_solger_state == "y" || $action_delete_solger_state == "yes" ]]
                    then
                    rm /etc/solger/$1 && success_write "Suppresion du fichier $1 réalisé avec succes" || fail_write "Le fichier $1 n'existe pas"
                    else
                        important_write "Suppresion du fichier $1 annulé"
                    fi
            fi
        else
            echo "suppresion directe"
        fi
    fi 
}
function verify_prerequist()
{
    if [ -d /etc/solger/sandbox/solana-crud-project ]
    then
        cd /etc/solger/sandbox/solana-crud-project && anchor build
        if [ $? == 1 ]
        then
            warning_write "Une erreur est survenue lors d'Anchor  Build"
            important_write "Tentative de résolution du problème"
            (cd /tmp ; wget https://mirror.umd.edu/ubuntu/ubuntu/pool/main/o/openssl/libssl1.1_1.1.1f-1ubuntu2_amd64.deb && sudo dpkg -i libssl1.1_1.1.1f-1ubuntu2_amd64.deb && rm libssl1.1_1.1.1f-1ubuntu2_amd64.deb )
            important_write "Reexécution ..."
            cd /etc/solger/sandbox/solana-crud-project && anchor build
        fi


    fi
    
}
function verify_docker_install(){
                get_answer "Vérification sur Docker"
                if ! [ -x "$(command -v docker)" ]
                then
                    fail_write "Docker n'est pas installé sur la machine"
                    important_write "Installation de Docker ..."
                    apt install docker.io -y && success_write "Installation de Docker réussie" && return 0 || fail_write "Erreur lors de l'installation de Docker " && return  1
                else
                    success_write "Détection d'une installation de Docker"
                    return 0;
                fi   
}

#Pemret un meilleur management des containers avec une gestion des erreurs plus poussés.
function docker_manager(){
                important_write "Installation de l'environnement de développement Solana sur Docker"
                important_write "Vérification de l'image"

                function docker_install_container(){
                    important_write "Création du container $1 en cours ...."
                    docker run -itd --name $1 $2 /bin/bash  && docker ps -a && success_write "Container créer avec succès " && return 0  || fail_write "Echec de la création du container $1" && return 1
                }
                function docker_rm_container(){
                    important_write "Suppresion du container $1 en cours ...."
                    docker stop $1 && docker rm $1 && green_write_sucess "Suppresion du container $1 réussie"  && docker ps -a &&  return 0 || fail_write "Une erreur est survenue lors de la suppresion du container $1 Utilisez Docker CLI" && return 1
                }


function docker_download_img(){
    echo "L'url est =======> " $1
    echo "Le fichier à télécharger est ======> " $2
    (cd /etc/solger ; wget $1 && verify_existant_element e $2 0 1 )
}

function docker_install_img(){ #789 
    echo "Tar Images===================> " $1
    echo "Name img===================> " $2
    echo "Container Name ======>" $3
    echo "URL ===============>" $4

    function process_run_img()
    {
        (cd /etc/solger ; important_write " Importation du container ... " &&  docker import $1 $2:latest && success_write "Container importer avec  succès" && docker_install_container $3 $2 && return 0 || fail_write " Installation échoué "  && return 1)
    }

    if verify_existant_element e $1 0 1
    then
        process_run_img $1 $2 $3
    else
        echo "Téléchargement de l'image $1"
        docker_download_img $4 $1 && process_run_img $1 $2 $3
    fi

}
                function docker_process()
                {
                    if [[ $1 == "reinstall" && $2 == "images" ]]
                    then
                        verify_docker_env images $3 0 1 && green_write_sucess "Successfull" &&  docker_rm_container $3    
                    elif [[ $1 == "reinstall" && $2 == "container" ]]
                    then
                        #echo "Container à supprimer est à réinstaller ======================> " $3
                        verify_docker_env container $3 0 1 && green_write_sucess "Détection du container" &&  docker_rm_container $3 && success_write "Suppresion du container $3 réalisé avec succès" && important_write "Installation du container $3 ...." && docker_install_container $3 $4
                        #echo "images name =========================>" $4
                        #echo "container name =========================>" $3
                    else
                        warning_write "Erreur interne rencontré"
                    fi
                }

                function purge_fail_docker()
                    {
                        #warning_write "Exec Purge Fail"
                        echo $1
                        echo "Images" $2
                        echo "Container Name" $3
                        italic_write_warning "Souhaitez-vous reinstaller le container $2 ?" 
                        important_write "Entrer (n) pour quiter ou alors la commande ====>  {  $1 }  pour continuer "
                        read do_action
                        if [ "$do_action" == "$1" ]
                        then
                            warning_write "Reinstallation du container $2"
                            docker_process reinstall container $2 $3
                        else
                            warning_write "Opération de reinstallation annulé"
                        fi
                    }

                function fail_process_docker() #commands img container-name
                {
                    fail_write "Des installation sont déjà présente"
                    italic_write "Détection des problèmes ...."
                    (cd /etc/solger ; node loader.js)  #=======================================================> Charge le loader
                    if verify_docker_env images $2 0 1 && verify_docker_env container  $3 1 0
                    then # si on envoie du 0
                       warning_write "Fail process Docker ==> Then 1"
                       echo "receive 0"
                       italic_write "Réparation système ...."
                       echo "Images  =================================>" $2
                       echo "Container Name ==========================>" $3
                       docker_install_container $3 $2 # container_name img
                    elif verify_docker_env container $3 0 1 #Par défauts => verify_docker_env container $3 0 0
                    then
                            warning_write "Fail process Docker ==> Then 2"
                            echo "Le container existe déjà"
                            echo "Commands  =================================>" $1
                            echo "Images  =================================>" $2
                            echo "Container Name ==========================>" $3                          
                            purge_fail_docker "$1" $3 $2
                    else
                        #Dans le cas ou ni l'image ou ni le container existe
                        echo "receive 1"
                        #echo "TAR_IMAGES =========================>" $4
                        docker_install_img $4 $2 $3 $5
                    fi                                                                                       
                }
                #Cette condition est le coeur de la fonction de Docker Manage, sa va permettre de gérer les diffèrents cas par exemple : déjà installé ou alors juste image instalé etc...
                if verify_existant_element e solana-img.tar 0 0   #Créer une fonction general_manager-container
                then 
                                        #ORIGINAL_IMG="solana-img-snapshot"
                                        #URl_DOWNLOAD="https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solana-img.tar"
                                        ORIGINAL_IMG=$1
                                        URl_DOWNLOAD=$2
                                        TAR_IMAGES=$3


                                        ORIGINAL_CONTAINER=$( echo $ORIGINAL_IMG | sed -e "s/img/vm/" )
                                        success_write "Fichier tar déjà existant" 
                                        #Si c'est déjà existant alors il faut l'installer
                                        important_write "Installation de Solana Snapshot Program"
                                        if verify_docker_env images "solana-img-snapshot " 0 1  && verify_docker_env container "solana-vm-snapshot" 0 1 #Si sa existe on envoie du 1
                                        then
                                            green_write_sucess "Tout est déjà installé"
                                            fail_process_docker "solger get solana-env --docker --snapshot --force"  $ORIGINAL_IMG $ORIGINAL_CONTAINER $TAR_IMAGES $URl_DOWNLOAD
                                        else
                                            fail_write  $ORIGINAL_CONTAINER
                                            fail_process_docker "solger get solana-env --docker --snapshot --force"  $ORIGINAL_IMG $ORIGINAL_CONTAINER $TAR_IMAGES $URl_DOWNLOAD
                                            #verify_docker_install   && install_snapshot_docker "solana-img.tar" $CONTAINER_NAME "solana-img" && success_write " Installation réussie " || fail_write " Installation échoué "
                                        fi
                else 
                                        important_write "Solger Docker Manage n'a détecté aucun problème"
                fi
}

#conception #test #déploiement dev (localhost) #déploiement preprod (devnet) #déploiement prod (mainet)
#Permet de de mettre une sorte "empreinte" dans le dossier de destination afin qu'on se fie pas uniquemeny qu'à solger_state pour les vérification
function get_finger()
{
     touch $1
}

#Pour le fichier lib.rs
STATE_FILE="/etc/solger"
MERGE_ORIGIN="/etc/solger/sandbox/myproject/programs/myproject/src/lib.rs"
MERGE_DEST="/etc/solger/sandbox/solana-crud-project/programs/solana-crud-project/src/lib.rs"
CRITICAL_FINGER="/etc/solger/sandbox/solana-crud-project/"
CRITICAL_SEQUENCES="libRsDone"

function merge_project(){
     warning_write "L'élement à vérifier =========================================> functional_crud_$5:true"
    function copy_merge_project_operation(){
        verify_state "solana{functional_crud_$5:true}"
        ( cd "$3" ; get_finger $4)
        green_write_sucess "Opération de copie réalisé avec succès   Backend(lib.rs) de Myproject vers Backend(lib.rs) de solana-crud-project"
    }
    if grep -Fxq "solana{functional_crud_$5:true}" /etc/solger/solger_state "$1" && verify_existant_element__v3 e "$4""$5" 0 1
    then
        green_write_sucess "La copie backend à déjà été effectué"
    else
            if verify_existant_element__v3 e "$4""$5" 0 1 && ! grep -Fxq "solana{functional_crud_$5:true}" /etc/solger/solger_state
            then
                (cd /etc/solger ; node loader.js)
                italic_warning_write "Le fichier solger_state semble être supprimé ainsi la valeur solana{functional_crud_$5:true} n'est pas référencé dans le fichier "
                italic_warning_write "Une nouvelle copie peut écraser le fichier lib.rs dans le répertoire de travail principale . Une sauvegarde est recommandé via la commande solger get scp --push-backup"
                italic_warning_write "Voulez-vous continuer ? "
                read action;
                if [ "$action" == "y" ]
                then
                    important_write "Lancement de l'opération"
                    cp "$2" "$3"
                    verify_state "solana{functional_crud_$5:true}"
                    ( cd "$4" ; get_finger $5)
                    green_write_sucess "Opération de copie réalisé avec succès   Backend(lib.rs) de Myproject vers Backend(lib.rs) de solana-crud-project"
                else
                    success_write "Anulation de l'opération"
                fi
            else
                    cp "$2" "$3"
                    verify_state "solana{functional_crud_$5:true}"
                    ( cd "$4" ; get_finger $5)
                    green_write_sucess "Opération de copie réalisé avec succès   Backend(lib.rs) de Myproject vers Backend(lib.rs) de solana-crud-project"
            fi

    fi
}
#Merge Project Shortcut est un raccourci de MergeProject sa va permettre de faire les transfert de fichier comme lib.rs du old Backend vers le new Backend




#Cette fonction nous permet de générer une paire de clé pour le programme et de l'injecter dans le fichier lib.rs
function  finalizingProgramAddress(){
"" | solana-keygen new -o /etc/solger/sandbox/solana-crud-project/target/deploy/solana_crud_project-keypair.json --force
PROGRAM__ADDRESS=$( solana address -k /etc/solger/sandbox/solana-crud-project/target/deploy/solana_crud_project-keypair.json)
#PROGRAM__ADDRESS=$( "" |  solana-keygen new -o /etc/solger/sandbox/solana-crud-project/target/deploy/solana_crud_project-keypair.json )
success_write "L'adresse  du smart contract  est $PROGRAM__ADDRESS" 
important_write "Mise à jour de l'adresse dans le fichier lib.rs "
if verify_existant_element__v3 e /etc/solger/sandbox/solana-crud-project/programs/solana-crud-project/src/lib.rs
then
    sed -i "s/4nErQniDTU3B8KXpUZfcJ135EyHXDMYVAr6jeW5jDFgL/$PROGRAM__ADDRESS/g" /etc/solger/sandbox/solana-crud-project/programs/solana-crud-project/src/lib.rs && green_write_sucess "Mise à jour de l'adresse du programme réussi"
    sed -i "s/Fg6PaFpoGXkYsidMpWTK6W2BeZ7FEfcYkg476zPFsLnS/$PROGRAM__ADDRESS/g" /etc/solger/sandbox/solana-crud-project/Anchor.toml && green_write_sucess "Mise à jour dans le fichier Anchor.toml effectué"
else
    fail_write "Erreur de modification"
fi 
 
}
#Cette fonction exécute Anchor Build afin de générer l'IDL correspondant à lib.rs
finalizingMergeProjectBackend(){
            #(cd /etc/solger/sandbox/solana-crud-project ; anchor build) 
            verify_prerequist && green_write "Regeneration de l'IDL réussie"
}
#finalizingProgramAddress => Cette fonction doit être injecté avant l'anchor Build afin qu'Anchor la mette dans l'IDL lors du BUILD.
function mp_short(){
            merge_project  "$STATE_FILE" "$MERGE_ORIGIN" "$MERGE_DEST" "$CRITICAL_FINGER" "$CRITICAL_SEQUENCES"
            #On modifie myproject en solana-crud-project pour pas générer d'erreur par la suite
            sed -i 's/myproject/solana_crud_project/g' /etc/solger/sandbox/solana-crud-project/programs/solana-crud-project/src/lib.rs
            merge_project  "$STATE_FILE1" "$MERGE_ORIGIN1" "$MERGE_DEST1" "$CRITICAL_FINGER1" "$CRITICAL_SEQUENCES1"
            sed -i 's/myproject.js/solana-crud-project.js/g' /etc/solger/sandbox/solana-crud-project/Anchor.toml
            sed -i 's/myproject/solana_crud_project/g' /etc/solger/sandbox/solana-crud-project/Anchor.toml
            merge_project  "$STATE_FILE2" "$MERGE_ORIGIN2" "$MERGE_DEST2" "$CRITICAL_FINGER2" "$CRITICAL_SEQUENCES2"
            #On adapte le fichier myproject.js a solana-crud-project.js avec les noms qui vont bien
            sed -i 's/Myproject/SolanaCrudProject/g'  /etc/solger/sandbox/solana-crud-project/tests/solana-crud-project.js
            merge_project  "$STATE_FILE3" "$MERGE_ORIGIN3" "$MERGE_DEST3" "$CRITICAL_FINGER3" "$CRITICAL_SEQUENCES3"
            finalizingProgramAddress
            finalizingMergeProjectBackend                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
}
#mp_short

#function functional_crud_solana_program_front-end() Cette fonctions se charge de mettre de metrte à jour les IDL du back vers front
REQ1="/etc/solger/sandbox/solana-crud-project/target/idl/ solana_crud_project.json"
REQ2="/etc/solger/sandbox/frontend/myproject/src/ idl.json"
CRITICAL_ORIGIN="/etc/solger/sandbox/solana-crud-project"
function functional_crud_solana_program_front-end() {
    italic_write " Origin Path => $1"
    italic_write " Destination Path => $2"
    ORIGIN_FILE=$(echo $1 | sed  "s/ //g")
    DEST_FILE=$(echo $2 | sed  "s/ //g")
    ORIGIN_FOLDER=$(echo $1 | sed "s/\s.*$//" )
    DEST_FOLDER=$(echo $2 | sed "s/\s.*$//" )
    function rsc(){
        important_write "Tentative de reexécution ..."
        functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"
    }
    if verify_existant_element__v2 e $1 0 1  && verify_existant_element__v2 e $2 0 1  #On teste si les fichiers existent
    then
        italic_write_warning "=================================================================   Case  1"
        echo $ORIGIN_FILE
        echo $DEST_FILE
        verify_hash  "$ORIGIN_FILE" "$DEST_FILE" &&  warning_write "Mise à jour du front-end" && cp "$ORIGIN_FILE" "$DEST_FILE" && success_write "Mise à jour du front end réussie"
    #avec le elif la le dossier n'existera pas si anchor build n'est pas au préalable exécuter au moins une fois.
    elif verify_existant_element__v3 d $3 0 1  && verify_existant_element__v3 e $ORIGIN_FILE 1 0 d #Si l'IDL n'existe pas dans le fichier d'origine
    then
        italic_write_warning "=================================================================   Case  2"
        important_write "Regeneration du fichier IDL dans le dossier $3 " 
        (cd $3 ; mp_short ) && green_write_sucess "Regéneration du fichier IDL réussie" && rsc
                  #Intégration de mp_short afin de gérer l'anchor build correctement ...
    else
        warning_write "Les dossiers n'existent pas"
        #verify_existant_element__v3 d $DEST_FOLDER 1 0 && (cd /etc/solger ; rm -r sandbox) && get_struct scp && restart_crud #Si le dossier de destination frontend/myproject existe pas on le retélécharge
        (cd /etc/solger ; node loader.js)
        if  verify_existant_element__v3 d /etc/solger/sandbox 1 0
        then                         #( scp pour  solana-crud-project )
                    ( cd /etc/solger ; get_struct scp  deploiement_mode ) && (cd /etc/solger/sandbox && anchor init solana-crud-project ) && rsc # RSC réexécute la fonction avec les paramère fournie pour vérifier si il y'a changement des dernière
        else
                    warning_write "La Sanbox Existe mais solana-crud-project n'existe pas"
                    (cd /etc/solger/sandbox && anchor init solana-crud-project ) && rsc
        fi
    fi
}
#functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN" #Critical Origine fait ici référence au chemin critique de vérification si celui n'existe pas Anchor va le récréer

function get_struct(){
    echo $1
    echo $2
    echo $3
        if [[ $1 == "solana-env" && $2 == "" ]] || [[ $1 == "solana-env" && $2 == "--update" ]]
        then
                    echo "Installation d'un environnement de dévellopement Solana"
                    if test -f "solana-env-linux.v2.sh"
                    then
                        echo "Mise à jour du script d'installation Solana ...";
                        rm solana-env-linux.v2.sh;
                        wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solana-env-linux.v2.sh && chmod +x solana-env-linux.v2.sh && bash solana-env-linux.v2.sh &&   source ~/.bashrc && export PATH="/root/.local/share/solana/install/active_release/bin:$PATH" && export PATH="$HOME/.cargo/bin:$PATH";
                        verify_state solana-env{local_env_install:true};
                    else
                        echo "Téléchargement du script d'instalation ..."
                        wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solana-env-linux.v2.sh && chmod +x solana-env-linux.v2.sh && bash solana-env-linux.v2.sh &&   source ~/.bashrc && export PATH="/root/.local/share/solana/install/active_release/bin:$PATH" && export PATH="$HOME/.cargo/bin:$PATH";
                        verify_state solana-env{local_env_install:true};
                    fi
        eval "$(cat ~/.bashrc | tail -n +10)"     
        elif [[ $1 == "solana-crud-program" && $2 == "" || $1 == "scp" && $2 == "" || $1 == "scp" && $2 == "deploiement_mode" ||  $1 == "solana-crud-program" && $2 == "deploiement_mode"  ]]
        then
                    if [ -d "/etc/solger/sandbox" ]
                    then
                        success_write  "Le MVP CRUD est déjà installé"
                        italic_write "Executer la commande solger get solana-crud-program --update afin de mettre à jour ou solger start scp pour démarrer le programme"
                    elif [ -e "/etc/solger/sandbox.tar" ]
                    then
                        echo "Le fichier MVP CRUD à été trouvé "
                        echo "Décompression ... "
                        (cd /etc/solger ; tar -xvf sandbox.tar)
                        if [  "$2" == "deploiement_mode"  ]
                        then
                            italic_warning_write "Exécution de deploy_struct en mode deploiement"
                            (cd /etc/solger ; tar -xvf sandbox.tar)   
                        else
                            (cd /etc/solger ; tar -xvf sandbox.tar) &&  functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"
                        fi
                    else
                        if [  "$2" == "deploiement_mode"  ]
                        then
                            italic_warning_write "Exécution de deploy_struct en mode deploiement"
                            install_crud
                        else
                            install_crud &&  functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"
                        fi
                    fi

        elif [[ $1 == "solana-env" && $2 == "--docker" && $3 == "" ]]
        then
            echo "Installation de l'environnement de développement Solana sur Docker"
            verify_state solana-env{docker_env_install:true}
            apt-get update && apt install wget -y && apt install git -y && apt-get install docker.io -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/Dockerfile && docker build -f Dockerfile -t solana-img .;
        elif [[ $1 == "solana-env" && $2 == "--docker" && $3 == "--snapshot" && $4 == "" || $1 == "solana-env" && $2 == "--docker" && $3 == "--s" && $4 == "" || $1 == "solana-env" && $2 == "--d" && $3 == "--s" && $4 == "" || $1 == "sol-env" && $2 == "--d" && $3 == "--s" && $4 == "" || $1 == "sol-env" && $2 == "--d" && $3 == "--snapshot" && $4 == "" ]]
        then
                 docker_manager "solana-img-snapshot" "https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solana-img.tar" "solana-img.tar"
        elif [[ $1 == "solana-crud-program" && $2 == "--reset" && $3 == "" || $1 == "scp" && $2 == "--reset" && $3 == "" ]]
        then
            echo "Réinstallation de Solana Crud Program"
                                                if [ -e /etc/solger/sandbox.tar ]
                                                then
                                                    echo "Le fichier sandbox.tar existe"
                                                    verify_backup sandbox #Soccupe de mettre le dossier en backup.
                                                    rm -r /etc/solger/sandbox/
                                                    echo "Extraction ...."
                                                     (cd /etc/solger ; tar -xvf sandbox.tar) &&  functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"
                                                else
                                                    echo "Le fichier sandbox.tar n'existe pas"
                                                    install_crud &&  functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"
                                                fi
        elif [[ $1 == "solana-crud-program" && $2 == "--reset" && $3 == "--work" || $1 == "scp" && $2 == "--reset" && $3 == "--w" || $1 == "scp" && $2 == "--reset" && $3 == "--work"  ||$1 == "scp" && $2 == "--r" && $3 == "--w" ]]
        then
            echo "Réinstallation de Solana Crud Program avec l'option --work"
                                                if [ -e /etc/solger/sandbox.tar ]
                                                then
                                                    echo "Le fichier sandbox.tar existe"
                                                    verify_backup sandbox #Soccupe de mettre le dossier en backup.
                                                    rm -r /etc/solger/sandbox/
                                                    echo "Extraction ...."
                                                     (cd /etc/solger ; tar -xvf sandbox.tar) &&  functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN" &&  deploy_struct --l --exec-test && start_server_struct serve
                                                else
                                                    echo "Le fichier sandbox.tar n'existe pas"
                                                    install_crud &&  functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN" && deploy_struct --l --exec-test && start_server_struct serve
                                                fi
        elif [[ $1 == "solana-crud-program" && $2 == "--restaure" || $1 == "scp" && $2 == "--restaure" || $1 == "scp" && $2 == "--r" ]]
        then
            echo "Restauration de solana-crud-program"
            if [ -d /etc/solger/backup/sandbox ]
            then
                   echo "Le dossier MVP à été trouvé en backup"
                   echo "Restauration"
                   rm -r /etc/solger/sandbox
                   cp -r /etc/solger/backup/sandbox /etc/solger/
                   #echo -e "\e[1mRestauration complètement effectué ✔️\e[0m"
                   success_write "Restauration complètement effectué"
            else
                    echo "Aucun dossier MVP trouvé en backup"
                    #echo -e "\e[1mRestauration Echoué ❌\e[0m"
                    fail_write "Restauration Echoué"
                    echo -e " Veuillez installer solana-crud-program via la commande \e[1msolana get solana-crud-program\e[0m"
                    echo -e "Au reset l'état du  MVP est sauvegarder en backup. Elle sera accessible via la commande \e[1msolana get solana-crud-program --reset\e[0m" 
            fi
        elif [[ $1 == "solana-crud-program" && $2 == "--push-backup" || $1 == "scp" && $2 == "--push-backup" || $1 == "scp" && $2 == "--p-b" ]]
        then
            if [ -d /etc/solger/sandbox ]
            then
                   echo "Insertion de l'état de la MVP en backup"
                   verify_backup sandbox
                   success_write "Backup de la MVP réalisé avec succès !"
            else
                    fail_write "L'environnement de dévellopement Sandbox n'existe pas"
            fi
        elif [[ $1 == "solana-env" && $2 == "--docker" && $3 == "--snapshot" && $4 == "--regen"  ]] #Commande pas encore fonctionnel
        then 
            echo $1
            echo $2
            echo $3
            echo $4
            #resolv_container 
            if [ $5 == "" ]
            then
                important_write "Regeneration du container sans snapshot ..."
                resolv_container solana-vm-snapshot && success_write "Création du container réussie"
            else
                important_write "Regeneration du container avec Snapshot"
                resolv_container solana-vm-snapshot && success_write "Création du container réussie"
            fi
            echo "do process"
        elif [[ $1 == "own"   ]] #Commande pas encore fonctionnel
        then 
            chmod -R 777 /etc/solger
            green_write_sucess "Obtention des droits d'accès réussie"
        else
            echo "Solger Command Error from  get_struct => Vérifier les commandes fournies"
        fi          
}
function install_crud(){
                        echo "Installaton du MVP CRUD"
                        (cd /etc/solger ; wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/sandbox.tar)
                        (cd /etc/solger ; tar -xvf sandbox.tar)
                        success_write "Installation du MVP CRUD Réussie"
}
function resolv_container()
{
    important_write "Exécution de resolv container ...."
    if [[ $5 == "--push--backup" ]]
    then
        important_write " Exportation du container $2"
    else
        important_write "Arrêt du container $6 "
        docker stop $1 && important_write "Supression du container $6" && docker rm $1 && success_write "Suppression réussie du contrainer $6"
    fi
    #docker get solana-env --docker --snapshot --regen --push--backup || docker get solana-env --docker --snapshot --regen 
}
function export_container
{
    (cd /etc/solger ; docker export -o $1 $2 )
}
function secure_copy(){
                    important_write "Copie du dossier $1 en backup"
                    cp -r /etc/solger/$1 /etc/solger/backup/ && success_write  "Copie réalisé avec succès"  && return 0 || fail_write "Une erreur est survenue" && return 1
}
function secure_delete(){
                    important_write "Suppresion du dossier $1 en backup"
                    rm -r /etc/solger/backup/$1 && green_write_sucess "Suppresion du dossier $1 en backup réalisé avec succès" && return 0 || fail_write "Une erreur est survenue" && return 1
}
function verify_backup(){
            if [ -d /etc/solger/backup ]
            then
                green_write_sucess "Dossier Backup existant"
                if [ -d /etc/solger/backup/$1 ]
                then
                    success_write "Le dossier $1 existe dans le dossier Backup"
                    secure_delete $1
                    secure_copy $1 #Mise à jour du dossier solger en backup pour la nouvelle décompression
                else
                    echo "Le dossier $1 n'existe pas dans le dossier Backup"
                    secure_copy $1;
                fi
            else
                echo "Dossier Backup non existant"
                echo "Création du dossier Backup"
                ( cd /etc/solger/ ; mkdir backup )
                secure_copy $1;
            fi
}

function start_struct(){
        if [[ $1 == "solana-program-crud" && $2 == "--local-env" || $1 == "solana-program-crud" && $2 == "" || $1 == "scp" && $2 == "" || $1 == "scp" && $2 == "--local-env" ]]
        then
                echo "Démarrage de la version du program CRUD MVP Solana sur l'environnement locale"
                #cd /etc/solger/sandbox/frontend/myproject/ && npm run start
                #deploy_struct --l --exec-test && (cd /etc/solger/sandbox/frontend/myproject/ ; npm run start)
                deploy_struct --localhost --exec-test
                cd /etc/solger/sandbox/frontend/myproject/ && npm run start
        elif [[ $1 == "solana-program-crud" && $2 == "--docker-env" ]]
        then
                echo "Démarrage de la version du program CRUD MVP Solana sur l'environnement locale sur Docker"
                docker run -itd --name solana-vm solana-img /bin/bash
        elif [[ $1 == "solana-env" && $2 == "--docker" && $3 == "" ]]
        then
                echo "Démarrage de l'environnement de développement Solana dans Docker ..."
                #Ajout des commandes de vérification de démarre ex: si démarer => go instance
                    if [ $( docker ps -a | grep solana-vm | wc -l ) -eq 1 ] && [$3 == ""]
                    then
                            echo "Le container Solana-VM est installé"
                            echo "Démarrage ..."
                            docker start solana-vm

                    else
                            echo "Creation du container Solana-VM"
                            docker run -itd --name solana-vm solana-img /bin/bash
                    fi
        elif [[ $1 == "solana-env" && $2 == "--docker" && $3 == "--it" ]]
        then
                echo "Démarrage de l'environnement de développement Solana dans Docker ..."
                #Ajout des commandes de vérification de démarre ex: si démarer => go instance
                    if [ $( docker ps  | grep solana-vm | wc -l ) -eq 1 ]
                    then
                            echo "Le container Solana-VM est installé et démarré."
                            echo "Obtention de l'instance"
                            docker exec -ti solana-vm /bin/bash

                    else
                            echo "Demarrage du container Solana-vm"
                            docker start solana-vm
                            echo "Obtention de l'instance"
                            docker exec -ti solana-vm /bin/bash
                    fi
        else
            echo "Solger Command Error from start_struct => Vérifier les commandes fournies"
        fi   
}
function verify_docker_env(){

                   if [ $1 == "container"  ]
                   then
                            if [[ $( docker ps -a --format '{{.Names}}' | grep -w "$2" | wc -l ) -eq 1 ]]
                            then
                                    success_write "Une installation du container $2 à été détecté"
                                    return $3
                            else
                                    warning_write "Le container $2 n'existe pas"
                                    return $4
                            fi
                   else
                            if [[ $( docker images | grep -w "$2" | wc -l ) -eq 1 ]]
                            then
                                    success_write "Une installation de l'image $2 à été détecté"
                                    return $3
                            else
                                    warning_write "L'image $2 n'existe pas"
                                    return $4
                            fi
                   fi
}
#verify_docker_env images "solana-img " 1 0

function versioning()
{
    echo "Version " $VERSION;
}
function solger_ls()
{
    ls /etc/solger
}
function read_struct(){
        if [ -z $1 ]
        then
                ls /etc/solger
        else
            if [[ -e /etc/solger/$1 ]]
            then      
                nano /etc/solger/$1             
            else
                 echo "Solger Command Error from resolv_struct :"
                fail_write "Le fichier à lire n'existe pas"
                echo "Voici la liste des fichiers dans le répertoire :"
                ls /etc/solger
            fi
            
        fi

}
function show_struct(){
    if  [ "$1" == "--program-address" ]
    then
        PROGRAM__ADDRESS=$(solana address -k /etc/solger/sandbox/solana-crud-project/target/deploy/solana_crud_project-keypair.json)
        important_write "L'adresse de Solana Crud Project est $PROGRAM__ADDRESS"
    elif [ $1 == "path" ]
    then
    important_write "Chemin du dossier de dévellopement front-end : \n  
    /etc/solger/sandbox/frontend/myproject/ "
        important_write "Chemin du dossier de dévellopement backend : \n  
    /etc/solger/sandbox/solana-crud-project/ "
    important_write "Chemin  du fichier rust :  \n
    /etc/solger/sandbox/solana-crud-project/programs/solana-crud-project/src/lib.rs
     "
    important_write "Chemin  du fichier test de rust :  \n
    /etc/solger/sandbox/solana-crud-project/tests/solana-crud-project.js
     "
    elif [ $1 == "logs" ]
    then
    solana logs
    elif [ $1 == "programs" ]
    then
    solana program show --programs
    elif [[ $1 == "logs" && $2 == "on" && "$3" != "" ]]
    then
        solana logs | grep "$3 invoke" -A 3
    elif [ $1 == "explain" ]
    then
        italic_write "
            Pour builder notre programme, il faut exécuter la commande : cargo build-bpf
            Solana nous indique ou il à stocker le binaire pour qu'on puisse le déployer en devnet ou en mainet.

            Maintenant si on exécute la commande : cargo build-bpf --manifest-path=./src/program/Cargo.toml --bpf-out-dir=dist/program
            On indique le chemin à utiliser lors de la construction de nos programme, ces chemins pointent vers les dépendances.
            On indique également le chemin de sortie qui est -bpf-out-dir , c'est le chemin dans laquelle pointera le binaire solana
            ainsi que la pair de clé du programme.
        "
    else
        fail_write "Une erreur est survenue DEPUIS /from_struct "
    fi
}
function resolv_struct()
{
        if [[ $1 == "npm" && $2 == "--build"  || $1 == "n" && $2 == "--b" ]]
        then
                echo "Résolution des problème de build"
                export BROWSER=""
                sysctl fs.inotify.max_user_watches=524288 && sysctl -p && n 16.13.1 && export NODE_OPTIONS=
        elif [[ $1 == "running" && $2 == "--docker-env" ]]
        then
               echo ""
        else
            echo "Solger Command Error from resolv_struct => Vérifier les commandes fournies"
        fi
}
function update_struct(){
    function get__f(){
        ( cd /etc/solger ; wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solger.sh && chmod +x solger.sh; )
        ( cd /etc/solger ; wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/deployment.sh && chmod +x solger.sh; )
        ( cd /etc/solger ; wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/loader.js && chmod +x loader.js; )
        (cd /etc  ;  wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solger-restart-init.sh && chmod +x solger-restart-init.sh;  )
        (cd /etc/solger  ;  wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/variables.sh && chmod +x variables.sh;  ) 
   
}
if [ -d "/etc/solger" ]
then
  echo "Detection d'une installation existante de Solger "
  echo "Mise à jour de Solger ..."
  verify_existant_element__v3 e  /etc/solger/solger.sh 0 1 && rm /etc/solger/solger.sh;
  verify_existant_element__v3 e  /etc/solger/deployment.sh 0 1 && rm /etc/solger/deployment.sh;
  verify_existant_element__v3 e  /etc/solger/loader.js 0 1 && rm /etc/solger/loader.js;
  verify_existant_element__v3 e  /etc/solger-restart-init.sh 0 1 && rm /etc/solger-restart-init.sh;
  verify_existant_element__v3 e  /etc/solger/variables.sh 0 1 && rm /etc/solger/variables.sh;
  get__f;
  bash /etc/solger/./solger.sh solger_init --version
  echo -e "\e[1mMise à jour de solger réussie ✔️\e[0m"

else
  echo "Installation en cours"
  mkdir /etc/solger;
  get__f
  bash /etc/solger/./solger.sh solger_init --version
  echo -e "\e[1mInstallation de solger réussie ✔️\e[0m"
fi

if grep -Fxq "#solger:true" ~/.bashrc
then
    echo "Alias already install"
else
    echo "#solger:true">> ~/.bashrc;
    echo "Creation des alias pour Solger";
    echo "alias edit-a='nano ~/.bashrc'">> ~/.bashrc;
    echo "alias src='source ~/.bashrc'">> ~/.bashrc;
    echo "alias solger='bash /etc/solger/./solger.sh solger_init'" >> ~/.bashrc;
    echo "alias sr='nano /etc/solger/solger_state'" >> ~/.bashrc;
    echo "alias solger-read='cat /etc/solger/solger.sh'" >> ~/.bashrc;
    echo "alias solger-repair='bash /etc/solger-restart-init.sh failled_init_solger'" >> ~/.bashrc;
    echo "#solger end alias">> ~/.bashrc;
    source ~/.bashrc;
fi
echo "Installation de Solger reussie !"
}
#On vérifie que le hash de l'IDL du front-end et du backend correspondent
function verify_hash(){
    origin=($(md5sum $1))
    path=($(md5sum $2))
    italic_write "Hash du fichier d'origine à comparer :  $origin "
    italic_write "Hash du fichier de destination à comparer : $path " 
    if [ "$origin" == "$path" ]
    then
        success_write "Aucun changement détecté"
        success_write "Les fichiers sont à jour"
        return 0
    else
        warning_write "Changement détecté"
        return 0
    fi
}
 dx="| ----------------------------------------------------------------------------------------------------------------  |"
path_deployment=" /etc/solger/sandbox/solana-crud-project"
function deploy_struct(){
       function rX(){
         functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"
       }
        if [[ $1 == "--localhost" && $2 == "" && $3 == ""  || $1 == "--l"  && $2 == ""  && $3 == ""  ]]
        then
                   functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"  && echo $dx && wallet_verificator --localnet && echo $dx && testing  $path_deployment --localnet && echo $dx && deploy__on__localhost  $path_deployment localhost && rX
        elif [[ $1 == "--localhost" && $2 == "--exec-test" && $3 == ""  || $1 == "--l"  && $2 == "--exec-test"  && $3 == "" ]]
        then
                   functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"  && echo $dx && wallet_verificator --localnet && echo $dx && testing  $path_deployment --localnet && echo $dx && deploy__on__localhost  $path_deployment localhost && rX 
        elif [[ $1 == "--localhost" && $2 == "--exec-test" && "$3" == "--force" || $1 == "--l"  && $2 == "--exec-test" && "$3" == "--f" || $1 == "--l"  && $2 == "--exec-test" && "$3" == "--force" ]]
        then
            warning_write "Execution en mode force 2"
            finalizingProgramAddress &&   functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN"  && echo $dx && wallet_verificator --localnet && echo $dx && testing  $path_deployment --localnet && echo $dx && deploy__on__localhost  $path_deployment localhost 
        elif [[ $1 == "--devnet" && $2 == "" || $1 == "--d" && $2 == "" ]]
        then
                   functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN" && wallet_verificator --devnet && testing  $path_deployment --devnet  &&  wallet_verificator --devnet  && deploy__on__devnet  $path_deployment  devnet && rX #Exécution de rx rapidement afin de mettre à jour les fichiers
        elif [[  $1 == "--devnet" && $2 == "--exec-test" ||  $1 == "--d" && $2 == "--exec-test" ]]
        then
                   functional_crud_solana_program_front-end    "$REQ1"    "$REQ2" "$CRITICAL_ORIGIN" && wallet_verificator --devnet && testing  $path_deployment --devnet  &&  wallet_verificator --devnet  && deploy__on__devnet  $path_deployment  devnet && rX
        else
            echo "Solger Command Error from deploy_struct => Vérifier les commandes fournies"
        fi
    if grep -Fxq "{advanced-verification::wallet:true}" /etc/solger/solger_state
    then
        important_write "Verification du wallet de prod"
        verify_external_adress $1
    else
        italic_write "Pas de vérification supplémentaire à effectuer"
    fi
}
# deploy_struct --d --exec-test
function directly_started_struct(){
        function recurvise_process(){
            important_write "Vérificationn des informations sur l'environnement de dévellopement"
            if grep -Fxq "solana-env{local_env_install:true}" /etc/solger/solger_state
            then
                success_write "Fichier state à jour"
                 success_write "Environnement de dévellopement déjà installé"
            else
                warning_write "Installation de l'environnement de dévellopement"   
                get_struct solana-env
            fi            
            echo "Vérificationn des informations sur l'environnement de dévellopement Sandbox"
        }
        if [[ $1 == "running" && $2 == "--local-env" && $3 == "" || $1 == "r" && $2 == "" && $3 == "" || $1 == "running" && $2 == "" && $3 == "" || $1 == "r" && $2 == "--local-env" && $3 == "" ]]
        then
            recurvise_process
            if [ -d "/etc/solger/sandbox" ]
            then
                success_write "Le dossier de dévellopment existe déjà "
                warning_write " Démarrage ..."
                deploy_struct --localhost --exec-test
                #start_struct solana-program-crud --local-env;
                start_server_struct serve
                
            else
                warning_write "Installation de l'environnement de dévellopement Sandbox"
                #986
                get_struct scp &&  deploy_struct --localhost --exec-test
                start_server_struct serve
            fi
        elif [[ $1 == "running" && $2 == "--local-env" && $3 == "--devnet" ||   $1 == "r" && $2 == "--d" || $1 == "r" && $2 == "--devnet" ]]
        then
            recurvise_process
            if [ -d "/etc/solger/sandbox" ]
            then
                success_write "Le dossier de dévellopment existe déjà "
                warning_write " Démarrage ..."
                deploy_struct --devnet --exec-test
                #start_struct solana-program-crud --local-env;
                start_server_struct serve
            else
                warning_write "Installation de l'environnement de dévellopement Sandbox"
                get_struct solana-crud-program &&  deploy_struct --devnet --exec-test
                start_server_struct serve
            fi     
        elif [[ $1 == "running" && $2 == "--docker-env" ]]
        then
               echo "Déploiement de tout l'environnement et du MVP sur Docker"


            #Docker verify instruction
            echo "Vérificationn des informations sur l'environnement de dévellopement"
            if grep -Fxq "solana-env{docker_env_install:true}" /etc/solger/solger_state
            then
                echo "Fichier state à jour"
                echo "Environnement de dévellopement Docker déjà installé"
            else
                echo "Installation de l'environnement de dévellopement Docker"   
                get_struct solana-env --docker 

            fi            
            echo "Vérification des informations sur l'environnement de dévellopement Sandbox de Docker"
               
        else
            echo "Solger Command Error from directly_started_struct => Vérifier les commandes fournies"
        fi
}
function init_solana_struct(){
        if [[ $1 == "solana" && $2 == "--localhost" ]]
        then   
            if grep -Fxq "solana{config_local_wallet:true}" /etc/solger/solger_state
            then
                success_write "Wallet locale Solana déjà configuré"
                important_write "L'addresse locale du Wallet est $(solana address) "
            else
                echo "Configuration du wallet Solana en cours"   
                "" | solana-keygen new
                echo "Configuration du Wallet Solana terminé"
                echo "Mise à jour du fichier Solger State"
                verify_state solana{config_local_wallet:true}
                echo "Fichier Solger State Mise à jour"
            fi  
        elif [[ $1 == "solana" && $2 == "--devnet" ]]
        then
                if  grep -Fxq "{config__minimal__address --devnet:true}" /etc/solger/solger_state 
                then
                        echo "Le fichier de registre Solger contient des adresses déjà configurées"
                        echo "Vérification  de l'adresse renseignée dans le registre de fichier"
                        read WALLET_ADDRESS;
                        search_adress_struct $WALLET_ADDRESS
                else
                        verify_state "{config__minimal__address --devnet:true}"
                        echo "Le fichier de registre Solger n'a aucune adresse configurée"
                        read WALLET_ADDRESS;
                        verify_state 'STATE_LIST__NUMBER:0'
                        search_adress_struct $WALLET_ADDRESS
                fi
        elif [[ $1 == "solana" && $2 == "--project" || $1 == "sol" && $2 == "--p" ]]
        then
                important_write "Création d'un nouveau projet Solana via Anchor"
                if  [ -z $3 ]
                then
                       fail_write " Veuillez renseigner le nom du projet Solana que vous voulez créer"
                       important_write "Exemple: solger init sol --p myproject "
                else
                       anchor init $3    
                fi
                
        fi                 
}           
function search_adress_struct(){
                if grep -Fxq $WALLET_ADDRESS /etc/solger/solger_state
                then
                    echo "L'adresse existe déjà dans la liste "
                    echo "Récupération des informations ... "
                    CHECK_BALANCE=$(solana balance $WALLET_ADDRESS | sed  -r 's/(.{1}).*/\1/')
                    if [ $((CHECK_BALANCE )) -lt 2 ]
                    then
                        important_write "Solde inssufisant pour l'intéraction"
                        important_write "Ajout de fonds"
                        solana aidrop 2 $WALLET_ADDRESS
                    fi
                else
                    if  grep -Fxq  "9FNUeTzgSsHxukP9BYLqBdesRhcfbbLii878aoEQyh43" /etc/solger/solger_state #Etape de vérification intermediaire
                    then
                        echo "L'adresse existe"
                        echo '{ "wallet_address_" : "9FNUeTzgSsHxukP9BYLqBdesRhcfbbLii878aoEQyh43"}' >> /etc/solger/solger_state
                    else
                        fail_write "Echec de l'écriture"
                        echo '{ "wallet_address_" : "9FNUeTzgSsHxukP9BYLqBdesRhcfbbLii878aoEQyh43"}' >> /etc/solger/solger_state


                    fi
                    
                fi
}     

get_privilege_wallet(){
     PREFERED_WALLET=$(cat /etc/solger/solger_state | grep PRIVILEGES_WALLET_FAVORITE)
     echo $PREFERED_WALLET
}
set_privilege_wallet_address(){
    if [ "$1" == "" ]
    then
        warning_write "Veuillez renseigner une adresse"
    else

        if solana balance $1 > /dev/null 2>&1
        then
                if  grep -Pq "$1" /etc/solger/solger_state #Si le wallet existe dans le fichier solger state
                then
                    italic_write "Adresse existant dans la base"
                    PREFERED_WALLET=$(cat /etc/solger/solger_state | grep PRIVILEGES_WALLET_FAVORITE)
                    sed -i "s/$PREFERED_WALLET/PRIVILEGES_WALLET_FAVORITE:$1/g" /etc/solger/solger_state
                    success_write "Modification du wallet favori effectuer avec succès"

                else
                    fail_write "L'adresse n'existe pas dans la base, veuillez l'ajouter"
                fi
        else
              fail_write "L'adresse entrer n'est pas valide veuillez re vérifier"
        fi
    fi

}
#set_privilege_wallet_address BktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJR
function set_struct(){
    echo $1
    if [[ "$1" == "prefered-wallet" ]]
    then
        set_privilege_wallet_address $2
    elif [[ "$1" == "advanced" && "$2" == "wallet" && "$3" == "verification" && "$4" == "true"   ]]
    then
        if grep -Fxq "{advanced-verification::wallet:false}" /etc/solger/solger_state
        then
            sed -i 's/{advanced-verification::wallet:false}/{advanced-verification::wallet:true}/g' /etc/solger/solger_state
        else
                    verify_state "{advanced-verification::wallet:true}"
        fi
    elif [[ "$1" == "advanced" && "$2" == "wallet" && "$3" == "verification" && "$4" == "false"   ]]
    then
        if grep -Fxq "{advanced-verification::wallet:true}" /etc/solger/solger_state
        then
            sed -i 's/{advanced-verification::wallet:true}/{advanced-verification::wallet:false}/g' /etc/solger/solger_state
        else
            fail_write "Aucunne configuration du wallet au niveau de la vérification n'a été trouvé "
        fi
    elif [[ "$1" == "solana" && "$2" == "environnement"   ]]
    then
            set_env $3 ;
    else
        echo "no process"
    fi
}



function specific_addr(){
     echo $1
    ADDR=$(cat solger_state | grep wallet_address_$1 | jq ".wallet_address_$1"  | sed 's/"//g')
    solana airdrop 2 $ADDR
}
#specific_addr addr_ABCEFGEAB

function verify_list_address(){
    super_validator start
    function adding_address(){
        echo "L'incrémentation à commencer"
        STATE_LIST__NUMBER=$(cat /etc/solger/solger_state | grep STATE_LIST__NUMBER)
        GET_VALUE_LIST_STATE_ADDRESS=$(echo ${STATE_LIST__NUMBER:19})  #Récupere la valeur de STAE_LIST_NUMBER
        echo "{ "\""wallet_address_$GET_VALUE_LIST_STATE_ADDRESS"\"" : "\""$1"\"" }" >> /etc/solger/solger_state
        GET_VALUE_LIST_STATE_ADDRESS__INTEGER__INCREMENT="$(($GET_VALUE_LIST_STATE_ADDRESS + 1))" #Incremente STATE_LIST_NUMBER à chaque appel
        sed -i "s/STATE_LIST__NUMBER:$GET_VALUE_LIST_STATE_ADDRESS/STATE_LIST__NUMBER:$GET_VALUE_LIST_STATE_ADDRESS__INTEGER__INCREMENT/g" /etc/solger/solger_state
        green_write_sucess "Adresse n°  $GET_VALUE_LIST_STATE_ADDRESS__INTEGER__INCREMENT  ajouté "
        sed -i 's/HEADER_LIST_WALLET:END//g' /etc/solger/solger_state
        echo "HEADER_LIST_WALLET:END" >> /etc/solger/solger_state
    }
    function verify_index_mode()
    {
        if  grep -Pq "STATE_LIST__NUMBER" /etc/solger/solger_state 
        then
            green_write_sucess "Adresse détecté"
        else
            italic_write "Aucunne adresse détecté dans la liste"
            italic_write "Insertion d'un index"
            echo "{configured_minimum_external_wallet:true}" >> /etc/solger/solger_state
            echo "STATE_LIST__NUMBER:0" >> /etc/solger/solger_state
            echo "PRIVILEGES_WALLET_FAVORITE:$1" >> /etc/solger/solger_state
            echo "HEADER_LIST__WALLET:STARTED" >> /etc/solger/solger_state
            #italic_write "Réexécution de la fonction"
            #verify_list_address $WALLET_ADDRESS
        fi
    }

    re_process(){
            solana balance $1 > /dev/nul 2>&1 && verify_index_mode $1 && adding_address $1 && green_write_sucess "Success" || fail_write "Echec de l'ajout de l'adresse veuillez vérifier que l'adresse est valide sur le réseau"
    }

    function launching_fonction(){
        if  grep -Pq "$1" /etc/solger/solger_state #Si le wallet existe dans le fichier solger state
        then
            fail_write "Impossible d'ajouter car l'adresse existe déjà"
        else
                        italic_write "Ajout de l'adresse"
                        re_process $1 #Sinon on l'ajoute. 
                        #Verify_index_mode => Ajoute un en-tête de début de répérage de la liste d'addresse dans le fichier solger_state
                        # state_list_number: 0 pour l'indexage des wallet, privilege_wallet_favorite : pour permettre d'utiliser un wallet de préference dans toute la lliste
        fi
    }
    if  [ "$1" == "--insert-addr" ]
    then
            #echo "BktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJR"
            solana balance $2 > /dev/nul 2>&1 && launching_fonction $2 || fail_write "Erreur : Vérifier l'adresse"
    else
            solana balance $2 #> /dev/nul 2>&1 && launching_fonction $NEW_WALLET_ADDRESS | fail_write "Erreur d'adresse"
    fi
}
#verify_list_address --insert-addr
function insert_struct(){
    echo $1
    echo $2
    echo $3
    echo $4
    if [[ "$1" == "wallet"  && "$2" == "addr" && "$3" != "--interaction" ||  "$1" == "wallet"  && "$2" == "address" && "$3" != "--interaction" ]]
    then
        warning_write "Mode 1 exec ..........."
        verify_list_address --insert-addr $3
    elif [[  "$1" == "wallet"  && "$2" == "addr" && "$3" == "--interaction" || "$1" == "wallet"  && "$2" == "address" && "$3" == "--interaction" ]]
    then
        warning_write "Mode 2"
        italic_warning_write "Veuillez renseigner une adresse de portefeuille"
        read wallet_from_user
        verify_list_address --insert-addr $wallet_from_user && return 0
    else
        fail_write "Erreur provenant de insert_struct" && return 1 
    fi
}

round() {
  printf "%.0f\n" $1
}
function verify_external_adress(){
        super_validator start

        #ReEcriture des commandes si l'utilisateur utilise des commandes abregés
        if [ "$1" == "--d" ] 
        then
            set_env --devnet > /dev/null 2>&1
        elif [ "$1" == "--l" ]
        then
            set_env --localnet > /dev/null 2>&1
        else
            set_env $1 > /dev/null 2>&1
        fi
        if grep -Fxq "{configured_minimum_external_wallet:true}" /etc/solger/solger_state
        then
                A=$(get_privilege_wallet)
                from_prefered=${A:27:50}
                BEFORE_PARSE=$( solana balance $from_prefered | sed  -r 's/SOL//g' )
                CHECK_BALANCE=$(round $BEFORE_PARSE)
                function reprocess_(){
                           green_write_sucess "Valeur de counter : $state_program_airdrop "
                            important_write "Valeur du portefeuille en prod $(solana balance $from_prefered)"
                            green_write "Opération  $state_program_airdrop / $wanted_solana_airdrop " 
                            ((state_program_airdrop=state_program_airdrop+1))
                            if [ $state_program_airdrop == $wanted_solana_airdrop ]
                            then
                                green_write_sucess "Fin de configuration du wallet Solana en  prod "
                                echo "{configured_minimum_external_wallet::full_airdrop:true}" >> /etc/solger/solger_state
                                important_write "Valeur du portefeuille en prod $(solana balance $from_prefered)"
                            fi
                }
                #On envoie un bon coup d'airdrop pour être tranquille un moment lors des interraction
                if grep -Fxq "{configured_minimum_external_wallet::full_airdrop:true}" /etc/solger/solger_state
                then
                     green_write_sucess "Wallet déjà initialisé"
                else
                        state_program_airdrop=0
                        wanted_solana_airdrop=3 #Pour le test
                        while [ $state_program_airdrop -le  $wanted_solana_airdrop ]
                        do
                           solana airdrop 10 $from_prefered &&  reprocess_ $state_program_airdrop $from_prefered $wanted_solana_airdrop
 
                        done
                fi

                if [ $((CHECK_BALANCE)) -gt 2 ]
                then
                    green_write_sucess "Font suffisant pour l'utilisation du wallet en prod"
                    green_write "Vous avez $((CHECK_BALANCE)) sol dans le portefeuille"
                else
                    important_write "Ajout de fonds dans le wallet "
                    solana airdrop 10 $from_prefered && success_write "Font ajouté avec succès"
                fi
        else
            #Si aucunne adresse n'est trouvé
            insert_struct wallet addr --interaction wallet_addr &&  verify_external_adress
        fi
}
#verify_external_adress
# insert_struct wallet addr wallet_addr  ||   insert_struct wallet addr --interaction wallet_addr



function install_struct()
{
    if [ "$1" == "terraform" ]
    then
        wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
        echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
        sudo apt update && sudo apt install terraform
    elif [ "$1" == "gcli" ]
    then
        sudo apt-get install python3.8 python3-pip -y
        (cd /home ; mkdir gcli)
        (cd /home/gcli ; curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-393.0.0-linux-x86_64.tar.gz)
        (cd /home/gcli ; tar -xf google-cloud-cli-393.0.0-linux-x86_64.tar.gz)
        (cd /home/gcli ; "Y" | ./google-cloud-sdk/install.sh)
    else
        fail_write "Erreur provenant de la structure install"
    fi
}

function try_debug()
{
    if [[ "$1" == "--help" ]]
    then
        italic_write "Missmatch Program ID : solana-keygen new -o your_path/target/deploy/your_project-keypair.json --force"
        italic_write "Unknow program tests via Anchor : Vérifier que le fichier de tests porte bien le même nom que celui qui est déclarer"
        italic_write "Provider Not available : Entrer la commande export BROWSER= "
        warning_write "Avant d'importer un fichier il faut remplacer toutes les occurences relatifs aux fichiers"
        warning_write "Exemple : anchor init pdx => cela donne par exemple Pdx dans le fichier générer TypeScript"
        italic_write "Ce quil faut faire lorsqu'on importe un nouveau c'est de remplacer toutes ancienne ref par Pdx"
    fi
}

function compile_struct(){
 green_write_sucess "Compilation du script réalisé avec succès !"
}

<<COMMENT
        Revoir la fonction wallet_verification celui modifie lenvironnement en localnet pour vérifier ainsi en 
        preprod les valeurs sont inexacte                                   
COMMENT
"$@"
