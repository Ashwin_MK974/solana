# Solger a Solana Manager

  
  
  
  

## Introduction

  
  
  

  

Solger est un service permettant de gérer avec faciliter les projets de la blockchain Solana.

  

  

  

Ce service permet par exemple de déployer un environnement de développement complet afin de créer des programmes Solana. Cela peut être effectué sur la machine locale ou Docker.

  

  

  

Il inclut d'une application MVP qui permet de réaliser les opérations minimales d'un CRUD. L'application est codée en React JS et fonctionne sur un serveur NodeJS.

  

  

Solger permet ainsi de démarrer directement la MVP avec les dépendances nécessaire sans avoir besoin de les installer au préalable. On a ainsi un environnement de développement prêt à l’emploi afin de commencer à développer.

  

  

Il intègre un panel permettent de déployer facilement des projet sur l’environnement de préproduction Devnet. Lors de ce déploiement, ce sont ainsi tous les fichiers nécessaires qui sont mis à jour afin que les points de terminaison frontend et backend puissent être opérationnel sans aucune opération manuelle .

  

  

  

## Installer Solana Manager

  

  

  

```
apt update && apt install wget -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/install.sh && chmod +x install.sh && bash install.sh && . ~/.bashrc
```
### Prérequis
Programme testé et utilisé sur une version Ubuntu 22.04

### Démarrer facilement
  
`solger directly running --local-env` 

Pour avoir directement l'environnement complet pour le dévellopement des dapps sur le réseau Solana. Celui-ci comprend l'environnement d'exécution des programmes Solana, le framework Anchor, l'installation des outils Rust, le gestionnaire de package Javascript NPM ainsi que le serveur NodeJS.

  

  

En utilisant directly running sans options supplémentaire, Solger est configuré pour utiliser par défaut l'environnement de développement localnet pour des raisons rapidité de déploiement.

Cette commande comprend également l'installation d'un MVP CRUD React JS et son lancement directement via le serveur NodeJS.

L'environnement complet de développement front-end et back-end est ainsi prêt à être utilisé.

  

  

  

  

La commande suivante permet également de réalisé ces actions

  

  


```
solger d r
```

    solger d r --devnet

Comme la commander solger d r est configuré pour fonctionner en localnet, en spécifiant l'option --devnet, la commande déploiera ainsi la MVP en devnet
 

  

## Fonctionnement de Solana Manager

Solger permet de déployer sur la devnet ainsi que l'environnement locale. Lors des déploiements, cela induit un changement de configuration de Solana. Par exemple lors de la configuration de l'environnement de Solana via la commande **solana config get --url localnet** cela implique une modification uniquement sur la configuration de Solana. Néanmoins dans les fichiers des programmes que l'on a construit via **anchor init** cela implique de modifier cette configuration dans le fichier Anchor.toml . Solger se charge ainsi d'effectuer ces modifications directement avant de déployer.

  

L'adresse du smart contract qui est générer est directement intégrer dans le fichier **lib.rs** mais également dans le fichier **Anchor.toml**.

  

  

  

Solger intègre également des fonctions permettant d'assurer le déploiement comme :

  

  

  

- **wallet_verificator** : Permet de vérifier que l'on dispose une adresse publique valide ainsi qu'assez de fonds dans le wallet avant de déployer. Si ce n'est pas le cas, cela entraine la création d'un nouveau **portefeuille** et/ou l'**ajout** des **fonds**.

- **set_env** : permet de modifier la configuration de **Solana** soit en **devnet** ou **localhost**, avec une propagation de ces modifications dans le fichier **Anchor.toml**

  

- **verify_hash** : permet de faire un comparatif des hash entre le back-end et le front-end si des hash différents sont détecté cela donne lieu à une mise à jour.

  

  

- **detect_solana_validator** : Cette fonction est importante, car elle permet de détecter l'exécution de **solana-test-validator**. Cette fonction renvoie une valeur que l'on peut personnaliser afin d'exécuter le reste des opération ou de les annuler via return 0 et return 1 dont les valeurs sont des variables que l'on peut définir. Combiné à solana_validator_manager on peut ainsi gérer avec facilité **solana-test-validator**.

  

- **solana_validator_manager** : Cette fonction permet de démarrer **solana-test-validator** en arrière plan. Elle permet également de l'**arrêter** en utilisant derrière la commande **kill**.

  

  

- **deploy_on_devnet** / **deploy_on_localhost** : permet de déployer le smart contract sur la devnet ou localnet avec des vérifications en amont. Par exemple si le validateur n'est pas en marche, il va l'activer afin de pouvoir déployer. Elle fait donc appelle à **detect_solana_validator** ainsi qu'à **solana_validator_manager**.

  

  

- **testing** : Exécute un test essentiellement local afin de gagner en rapidité et de ne pas exécuter des tests sur la devnet. Cette fonction fait également appel à des vérifications comme les fonctions **deploy_on_devnet** par exemple. En effet l'exécution de la commande **anchor test** nécessite à ce que le **validateur** ne soit pas démarré afin de permettre son exécution. Ainsi, le testing vérifie systématiquement l'activité de **solana-test-validator**. Lorsqu'elle est démarrée alors la fonction va tout simplement la désactiver pour permettre l'exécution de la commande **anchor test**.

  

  

- **functional_crud_solana_program_front-end** : Cette fonction permet de définir un socle de vérification à l'initialisation de Solger. A savoir par le **téléchargement** ou la détection du fichier **sandbox.tar**. L'**extraction** de ce fichier, la **création** d'un nouveau projet appelé **solana-crud-project**, la **copie** des fichiers nécessaires vers **solana-crud-project**. Elle permet également d'assigner une adresse au **programme** ainsi que de le **compiler** . Cette fonction fait apelle à des fonctions qui ne sont pas répertorié au sein du CLI de Solger, car elles peuvent causer une instabilité du smart contract si elle ne sont pas appelé dans un ordre de séquencement logique. Parmi ces fonctions, on y trouve **MergeProject** pour la copie des fichiers essentielle permettent de faire fonctionner le nouveau back-end et le front-end. Cette copie provient d'un projet déjà existant, mais qui est upgradé via **anchor init scp** afin d'obtenir de nouvelle mise à jour. Il y a également la fonction **FinalizingProgramAddress** pour la génération des adresses pour le smart contract. Enfin, il y a **FinalizingMergeProjectBackend** qui exécute derrière la commande **anchor build.** pour la compilation permettent de générer l'IDL. La fonction **functional_crud_solana_program_front-end** est particulière, car elle peut redémarrer automatiquement (*via la sous-fonction **rsc** qui est un raccourci avec les paramètres nécessaires*) afin de détecter les changements d'état jusqu'à arriver à la fonction de **verify_hash**pour la **copie** de l'IDL permettent cette fois-ci de faire fonctionner le **front-end**.

  

  

- **deploy_struct** : Fonction permettent de déployer quel que soit l'environnement. Elle fait également appel à des fonctions internes et externes. Par exemple pour un déploiement en **devnet** avec la réalisation des **tests** voici les **fonctions** qui sont appelés :

  

- set_env

  

  

- functional_crud_solana_program_front-end

  

  

- wallet_verificator

  

  

- testing : *Test en locale*

  

  

- deploy_on_devnet

- rX

rX est un alias de functional_crud_solana_program-front-end:

Utilisé cette fois-ci pour le comparatif des modifications des fichiers

car passe par un ensemble d'états de vérification.

  

  

  

  

# Utilisation de Solger

  



  

  

  
 
### Voici quelques commandes pour utiliser Solger:
    solger read

  Pour lire les fichiers de configuration automatisé de Solger

     solger get solana-env

Déploie l'environnement complet de dévellopement de dapps pour les programmes Solana

    solger show

Permet de voir les informations sur le smart contract: | L'option **-- program-address** : Permet de voir par exemple l'adresse du smart contract.*
    solger show programs

Permet de voir les programmes qui ont été déployés
    solger show path

Permet d'obtenir le chemin absolue du dossier de dévellopement front-end ainsi que le chemin du fichier rust backend et de test.
    solger show logs

Permet de voir les logs du fichier rust lorsque des tests sont lancer 

    solger show logs on _addr_smart_contract_

Permet de voir les logs d'un smart contract en particulier lors qu'on exécute des fonctions de celui-ci 

    solger show explain

Affiche quelques documentations
 
    solger create new solana-project my_project

Permet de créer des projets solana à l'état pure (sans Anchor).

    solger test solana version 1.x.x

Permet d'installer différentes versions de Solana afin de pouvoir tester les projets

    solger try --help

Permet d'afficher diffèrente solutions pour débugger des problèmes

Ces projets sont situés dans le répertoire /etc/solger/sandbox

    solger rinit
   Permet de réinitialiser à l'état **initial** le fichier solger_state


    solger manage-validator <stop || start || reset>
   Permet de gérer **solana-test-validator** dans une autre instance de terminal en CLI.

    solger set advanced wallet verification true
   Permet de réaliser des vérifications sur le wallet en prod. Cette fonctionnalité est optionnelle afin de permettre une gestion automatique 
   du wallet. Elle peut également être désactivée via la commande suivante :     solger set advanced wallet verification false

    
    
    solger set solana environnement <--localnet || --devnet>
    
    
Permet de changer l'environnement d'exécution de Solana que ce soit en devnet ou en localnet


    

    solger dp --localhost --exec-test --force
    
    

Permet de forcer le déploiement dans le réseau local en tentant de résoudre les problèmes rencontrés. Généralement, il s'agit du  <ProgramID>
    

   

  

  

    solger get solana-env --docker
Déploie l'environnement complet de développement de dapps pour les programmes Solana dans Docker. Cela est fait sur mesure en téléchargent les nouvelles dépendances nécessaires.

  
  

    solger get solana-env --docker --snapshot

Déploie l'environnement complèt via une image qui est télécharger. Ce sont ainsi des dépendances qui ne sont à jour.

  

  

  

    solger get solana-crud-program

 Déploie un folder avec le programme backend solana et front end react

  
  
  

    solger run server

Permet de lancer le serveur directement lorsque les prérequis en compilation sont effectués.


  

  
  
  
  

**Solger intègre également des fonctionnalités permettant de gérer les backups :**

  

  

  

    solger get solana-crud-program --reset

 Charge les modifications apporté dans le dossier actuel "solana-crud-program" vers le dossier Backup. Elle déploiera à nouveau le MVP à l'état initiale.

  

    solger get solana-crud-program --reset --work

Similaire à la commande : **solger get solana-crud-program --reset** mais lance le serveur web en même temps.

  

  

    solger get solana-crud-program --restaure

 
Charge les modifications apportées à solana-crud-program dans le dossier **Backup** vers l'instance **solana-crud-project** actuelle à savoir /etc/solger/sandbox". Les modifications précédemment apportées sont ainsi sauvegardées et éditables à nouveau.

  

  

  

  

A noter que des allias de ces commandes sont également possibles :

  

  

  

```
solger get scp --reset # scp pour solana-crud-program.
```

  

  

  

Solger permet également d'envoyer l'espace de travail actuel en backup, pour tester de nouvelle modification.

  

  

  

  

À la version actuelle, ce sont des sauvegardes complètes qui sont gérées par Solger ce qui engendre la nécessité d'avoir des marges en espaces disques en fonction des programmes qui sont développés.

  

  

  

```
solger get scp --push-backup #Envoie le MVP actuelle en backup
```

  

  

  

Une restauration est également possible une fois le MVP envoyé en backup.

  

  

  

```
solger get scp --restaure

#Restaure le MVP depuis le backup pour l'envoyer vers l'espace de travail actuelle.

```

  

## Déploiement des programmes Solana

Le déploiement sur la Devnet est possible via les commandes suivantes :

  

  

  

```
solger deploy --d # Pour le déploiement en préproduction

```

  

  

  

```
solger deploy --d --exec-test # Pour l'exécution des tests avant le déploiement en préproduction
```

  

  

  

  

Le déploiement en localhost est également possible :

  

  

  

```
solger deploy --l

```
```
solger deploy --l --exec-test
```

  

  

  

Ainsi en développent directement en backend, lors du build le frontend est directement mis à jour sans opérations supplémentaire.

  

  

  

  

Solger intègre également des commandes permettant de résoudre les problèmes de build via le gestionnaire de packages NPM.

  

  

L'interaction front-end est déjà programmé pour utiliser un backend établi sur l'environnement de test Devnet de Solana.

  

  

  

Url : https://explorer.solana.com/address/4nErQniDTU3B8KXpUZfcJ135EyHXDMYVAr6jeW5jDFgL?cluster=devnet.

  

  

  

  

L'utilisation en localnet est également possible via une configuration du CLI Solana.

  

  

  

  

Pour commencer à utiliser le manager solger il faut l'installer via la commande :

  

# Notes importantes à propos de Solger

  

### Fichier solger_state

Solger est un programme stateless. Pour gérer les données ainsi que l'état des sorties entres les différents appels de fonction, la gestion des interactions, etc. Solger créé automatiquement un fichier appelé **solger_state** dans le répertoire de solger. Ce répertoire est situé dans le dossier **/etc**. Dans ce fichier, est écrit l'état des appels des fonctions qui ont été réalisées. Cela permet ainsi de gagner en rapidité d'exécution lorsque des fonction interne ou commande CLI sont de nouveau exécuté.

Exemple de configuration du **solger_state** :

  


    solana-env{local_env_install:true}
    solana{functional_crud_libRsDone:true}
    solana{functional_crud_libAnchorDone:true}
    solana{functional_crud_libJsDone:true}
    solana{functional_crud_libPackageDone:true}
    solana{config_local_wallet:true}

Dans ce fichier Solger à réaliser les tâches suivantes:

  
  

- Copie des fichiers

- Installation de l'environnement de développement local

- Configuration du portefeuille locale de l'utilisateur.

Lors de la réexécution, ces étapes ne seront donc plus réalisés, car on l'état des actions sont inscrit dans ce fichier

  

### Troubleshooting `🤔`

Lorsque le programme Solger rencontre des erreurs qui ne peuvent être résolues via les commandes initiales du CLI. Une restauration du programme est possible via la commande **solger-repair**. Celui-ci fait appel à un autre script permettent de faire fonctionner solger sans modification des programmes qui ont été écrites par l'utilisateur.

Néanmoins la commande **solger-repair --force** permet de tout réinstaller en supprimant l'espace de travail de développement. Il est envisagé de faire une sauvegarde via : **solger get scp --push --backup**.

  

Le flag **- -force** supprime également le fichier solger_state. Toutes les étapes d'installations sont ainsi réinstallées.


### 📰 Testing
*Cette section décrit les nouvelle implémentations au sein de solger ainsi que la description des fonctions qui n'ont pas encore été implémenté.*

Ajout d'une fonctionalité permettant d'ajouter un vraie wallet dans une liste. Cette liste contient ainsi une liste de wallet que l'utilisateur peut ajouter.
Cette liste de wallet pourra être utiliser afin de pouvoir tester facilement diffèrent scènario d'utilisation dans le smart contract.
La liste de wallet se situe dans le fichier solger_state. Cette liste est écrite dans un format json afin de pouvoir récupérer les clé et valeur de manière efficace via l'outil **jq**.

Ainsi lors du déploiement du smart contract on peut ainsi directement injecté des fonds dans un ou plusieurs wallet afin de tester des cas d'utilisation possible.

Pour ajouter un wallet il faut exécuter la commande suivante:

    solger insert wallet addr <Wallet_Address>
  Lors de la première insertion ce wallet est considerer comme le wallet favori de l'utilisateur. Ce champs appelé **Distinguished** est insérer en en-tête de la liste d'adresse du fichier solger_state.
  Elle contient donc des métadonnée comme : la liste d'adresse qui ont été ajoutés ainsi que les différentes listes d'adresse. La fonction insère également un index de fin afin de savoir ou se situe la fin de la liste d'adresse.
Exemple de l'en-tête de début ainsi que la liste d'adresse et l'indexe de fin :

    STATE_LIST__NUMBER:3
    PRIVILEGES_WALLET_FAVORITE:BktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJR
    HEADER_LIST__WALLET:STARTED
    { "wallet_address_0" : "AktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJA" }
    { "wallet_address_1" : "BktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJB" }
    { "wallet_address_2" : "CktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJC" }
    HEADER_LIST_WALLET:END
Pour changer l'adresse favori par une celle existante dans la base il faut exécuter la commande suivante:

    solger set prefered-wallet <Wallet-Address>
Ainsi lors des déploiement ces cette adresse qui sera vérifie afin d'assurer la conformité des interactions.

Ajout de la fonction **specific_addr** qui permet de pointer vers un index de liste d'adresse et pouvoir récupérer l'adresse complète. Cette fonction pourra ainsi passer dans une boucle pour faire un full airdrop.
Cela pourra être faite via la commande:

    solger full airdrop 
Cette fonction specific_addr pointe donc sur un index comme on peut le voir :

    ADDR=$(cat solger_state | grep wallet_address_$1  | jq ".wallet_address_$1" | sed 's/"//g')
    #En paramère ce trouve l'argument d'index que pointe l'utilisateur 
    #Exemple sur "wallet_address_2" : "CktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJC"
    #Exemple d'apelle avec specic_addr 2 => En interne :  cat solger_state | grep wallet_address_2  | jq ".wallet_address_2"
    #Ce qui donne en sortie
    "CktmsFMmfGbxGxsvZW18oj6bVxxr4Tr3kQkSDkUxNkJC"
    #L'opérateur << sed 's/"//g' >> permet en faite d'enlèver les guillemet afin de pouvoir     
    effectuer du solana aidrop sans erreur de syntaxe.


# Installation sans gestionaire via CLI

  

  

  

  

L'installation de l'environnement de développement est disponible ici sans Solger directement en CLI via l'exécution de ces lignes de commandes. Cependant, l'exécution de ces scripts via le CLI ne gère en aucun cas l'état de l'environnement de développement. Pour tester ainsi de nouvelles fonctionnalités, ce sont des cycles complet qui sont ainsi revérifié.

  

  

  

Solger utilise néanmoins quelques-unes de ces commandes pour la partie d'installation de l'environnement sous Docker.

  

  

  

  

# Solana Virtual Machine

  

  

  

Environnement d'exécution de programme Solana

  

  

  

  

Ce projet inclut un container Docker qui fonctionne sous Ubuntu. Celui-ci contient un environnement d’exécution pour exécuter des programmes Solana, ainsi que divers outils de déploiement similaire à Solidity pour la paire Ethereum/BSC.

  

  

  

  

Solana y est installé par défauts ainsi que des outils similaires tels que :

  

  

  

- Anchor : L’équivalent de truffle

  

  

  

- Mocha : Framework de test

  

  

  

- Solana : CLI

  

  

  

- Rust : Langage utilisé par Solana

  

  

  

- Solana Web3JS : ¨Pour l’interaction de la partie front-end à backend.

  

  

  

  

Les variables d’environnement ont été placer dans bashrc pour le CLI de Solana et d'autre sourcer.

  

  

  

## Started on Docker Container

  

  

  

Pour commencer à utiliser la SVM, d'abord faire un clone du projet, puis importer le container.

  

  

  

  

```

apt update && apt install wget -y && apt install git -y && apt install docker.io -y && git clone https://gitlab.com/Ashwin_MK974/solana.git && cd solana && docker import solana-img.tar solana-img:latest

```

  

  

  

Pour exécuter le container :

  

  

  

```

docker run -itd --name solana-vm solana-img:latest /bin/bash


```

  

  

  

## Create your first project with the container

  

  

  

Pour créer un projet avec le container prêt à l'emploie on peut directement utiliser la commande

  

  

  

```
create-solana-project name___of___project

```

  

  

  

  

# Started on Build Container

  

  

  

Le container peut ne pas être totalement à jour, pour cela il est conseillé de faire un build de l'image qui contient les dernière dépendance à jour:

  

  

  

  

Pour cela il faut exécuter la commande:

  

  

  

```

apt-get update && apt install wget -y && apt install git -y && apt-get install docker.io -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/Dockerfile && docker build -f Dockerfile -t solana-img . && docker run -itd --name solana-vm solana-img /bin/bash

```

  

  

  

  

La commande renverra à la dernière ligne l'ID du container qui sera en exécution, pour l'utiliser exécuter la commande:

  

  

  

  

```

docker exec -ti ID

```

  

  

  

# Ready to use on Docker

  

  

  

Pour l'utiliser en mode "PnP" exécuter la commande :

  

  

  

```

apt-get update && apt install wget -y && apt install git -y && apt-get install docker.io -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/Dockerfile && docker build -f Dockerfile -t solana-img . && docker run -it --name solana-vm solana-img /bin/bash

```

  

  

  

  

## Remote folder Solana Dapps from Docker to VsCode

  

  

  

Pour avoir accès à l'environnement de fichier du container Docker qui s'exécute sur une autre machine virtuelle via VsCode, il faut ajouter ProxyJump suivi de l'id et de l'addresse IP de la machine hôte.

  

  

  

Par exemple ici l'addresse IP du container Docker est 172.17.0.2 qui s'exécute sur la machine hôte dont l'addresse IP est 119.223.208.210

  

  

  

Cela équivaut à la commande SSH : ssh -J root@119.223.208.210 root@172.17.0.2

  

  

  

  

```

Host 172.17.0.2

HostName 172.17.0.2

ProxyJump root@119.223.208.210

User root

```

  

  

  

## Remote folder Solana Dapps from Docker in GCP (Google Cloud Platform) to VsCode

  

  

  

Pour se connecter à GCP via ssh, on à besoin d'utiliser l'option -i de ssh.

  

  

  

-i pour identity file nous permet de nous connecter à la machine distante via un fichier de configuration spécifique.

  

  

  

  

Pour générer la clé SSH dans un fichier spécifique utiliser la commande suivante :

  

  

  

  

```

ssh-keygen -t rsa -f C:\Users\__path_to_private_keys -C username_account

```

  

  

  

Ainsi pour établir une connection SSH avec la machine il faut alors exécuter la commande suivante :

  

  

  

```

ssh -i __path_to_private_keys username_account@ip_address_of_vm

```

  

  

  

Exemple de connection à une instance de VM de GCP Compute Engine

  

  

  

  

```

Host 134.115.172.220

HostName 134.115.172.220

IdentityFile C:/Users/Rev9/ssh-instance/gcloud-ssh-vm

User rev9

```

  

  

  

## Vscode ==> GCP ==> Docker

  

  

  

Pour ce connecter directement à la machine Docker qui s'exécute sur GCP, on va configurer le fichier config du dossier .ssh comme suit :

  

  

  

  

```

ssh -t -i C:/Users/Ashwin/ssh-instance/gcloud-ssh-vm polyskill974@34.125.172.220 ssh root@172.17.0.2

```

  

  

  

  

## Export a snapshot of Solana Dapps as Docker Image

  

  

  

Pour exporter le container qui contient les fichiers de la Dapp Solana sous forme image il faut exécuter la commande suivante

  

  

  

  

```

docker export -o myContainer.tar nameOfContainer

#Exemple :

docker export -o solana-img.tar solana-vm

```

  

  

  

  

## Import Container of Snapshot Solana Dapps

  

  

  

Pour importer un container de Solana Dapps sous forme Image

  

  

  

```
docker import MyContainer.tar MyContainer:latest

#Exemple

docker import solana-img.tar solana-img:latest

```

  

  

  

  

## Directly Started on ENV Machine

  

  

  

Pour avoir l'environnement de dévellopement de Solana directement sur la machine hôte exécuter la commande suivante:

  

  

  

```

wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/solana-env-linux.v2.sh && chmod +x solana-env-linux.v2.sh && bash solana-env-linux.v2.sh && source ~/.bashrc && export PATH="/root/.local/share/solana/install/active_release/bin:$PATH" && export PATH="$HOME/.cargo/bin:$PATH"

```

  

  

  

  

## Troubleshooting on Anchor Testing

  

  

  

Lorsqu'on fait un anchor init, dans le fichier Anchor.toml, il faut remplacer la ligne de script test par celui-ci :

  

  

  

  

Il est aussi nécessaire de mettre à jour node js via l'utilisation de "n".

  

  

  

```

test = "yarn run ts-mocha -t 1000000 tests/**/*.ts"


```

  

  

  

## Troubleshooting on Docker && ENV Machine

  

  

  

Si solana-cli n'est pas reconu, il faudra mettre à jour la variable d'environnement PATH pour cela exécuter la commande suivante :

  

  

  

Egalement valable pour rust

  

  

  

```

export PATH="/root/.local/share/solana/install/active_release/bin:$PATH" && export PATH="$HOME/.cargo/bin:$PATH"


```

  

  

  

Erreur qui peuvent également être rencontré :

  

  

  

  

Error: Provider env is not available on browser

  

  

  

  

Cette erreur est causée, car la variable d'environnement est définie sur une valeur, qui n'existe pas dans l'environnement dans lequel on développe, il faudra donc la supprimer.

  

  

  

Pour cela, il faut modifier la variable d'environnement BROWSER via la commande :

  

  

  

```

export BROWSER=

```

  

  

  

  

On peut également tomber sur ce genre d'erreur lors de l'exécution de npm run start.

  

  

  

  

Error: ENOSPC: System limit for number of file watchers reached

  

  

  

  

Cette erreur survient lorsque le nombre de fichier à surveiller atteint la limite fixé par défaut.

  

  

  

Pour réparer cela il suffit alors de modifier la valeur de max_user_watches via la commande :

  

  

  

  

```

sudo sysctl fs.inotify.max_user_watches=524288 && sysctl -p


```

  

  

  

sysctl est utilisé ici pour modifier les paramètres du kernel.

  

  

  

  

Erreur également rencontré sur NodeJs:

  

  

  

Error: You must provide the URL of lib/mappings.wasm by calling SourceMapConsumer.initialize({ 'lib/mappings.wasm': ... }) before using SourceMapConsumer

  

  

  

  

Pour régler cela il faut installer une version antèrieure de NodeJS:

  

  

  

```

sudo npm install -g n && n 16.13.1

```

  

  

  

  

Si l'erreur Node Options apparait il faut exécuter la commande suivante :

  

  

  

  

```
export NODE_OPTIONS=

```
