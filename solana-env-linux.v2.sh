#!/bin/bash
apt-get update
if [ -d /etc/ssh ]
then
	echo "Service SSH installé"
else
	echo "Installation Service SSH" && apt install -y ssh && sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config && echo 'root:offline' | chpasswd
	
fi
apt install iputils-ping -y && apt install jq -y && apt install git -y &&  apt-get install -y pkg-config build-essential libudev-dev && apt-get install nodejs -y &&  apt-get install npm -y && npm install -g n && n 16.13.1  && apt install traceroute && apt install net-tools -y && apt install nano -y && apt install curl -y && apt install cargo -y && curl https://sh.rustup.rs -sSf | sh -s -- -y && export PATH="$HOME/.cargo/bin:$PATH" && . ~/.cargo/env && sh -c "$(curl -sSfL https://release.solana.com/v1.9.4/install)" && export PATH="/root/.local/share/solana/install/active_release/bin:$PATH" && echo "export PATH="/root/.local/share/solana/install/active_release/bin:$PATH"" >> ~/.bashrc && source ~/.bashrc && solana-install update && solana config set --url localhost && npm install --global yarn && cargo install --git https://github.com/project-serum/anchor --tag v0.24.2 anchor-cli --locked  && . ~/.bashrc && service ssh restart && echo "Version de Solana : " && solana --version && echo "Version d'Anchor : " &&  anchor --version && echo "Version de NodeJS : " && node  --version && echo "Version de NPM : "   && npm --version && echo "mise à jour de commande de build " && echo "alias resolv-build-npm='export BROWSER= && sysctl fs.inotify.max_user_watches=524288 && sysctl -p && n 16.13.1 && export NODE_OPTIONS=' " >> ~/.bashrc && . ~/.bashrc
